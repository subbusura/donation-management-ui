export const darkTheme = {
    palette: {
        type: 'dark', // Switching the dark mode on is a single property value change.
    },
};

export const lightTheme = {
    palette: {
        primary: {
            light: '#000000',
            //main: '#283593',
            //dark: '#001064',
            main: '#424242',
            dark: '#000000',
            contrastText: '#fff',
        },
        secondary: {
            light: '#000000',
            //main: '#283593',
            //dark: '#001064',
            main: '#424242',
            dark: '#000000',
            contrastText: '#fff',
        },
    },
    // typography: {
    //     // Use the system font instead of the default Roboto font.
    //     fontFamily: [
    //         '-apple-system',
    //         'BlinkMacSystemFont',
    //         '"Segoe UI"',
    //         'Arial',
    //         'sans-serif',
    //     ].join(','),
    // },
};
