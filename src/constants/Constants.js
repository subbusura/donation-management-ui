import MemberSubscription from "../member/MemberSubscription";
import MemberDocs from "../member/MemberDocs";
import MemberPayment from "../member/MemberPayment";
import MemberPaymentAdd from "./../member/MemberPaymentAdd"

export const gender = [
    { value: 'Male', label: 'Male' },
    { value: 'Female', label: 'Female' }
];

export const profileType = [
    { value: 'individual', label: 'Individual' },
    { value: 'company', label: 'Company' }
]

export const donationType = [
    { value: 'Zakath', label: 'Zakath', id: 'Zakath', name: 'Zakath' },
    { value: 'Sadaqa', label: 'Sadaqa', id: 'Sadaqa', name: 'Sadaqa' }
]

export const roles = [
    { id: 'admin', name: 'Admin' },
    { id: 'district admin', name: 'District Admin' }
]

export const paymentType = [
    { value: 'cash', label: 'Cash', id: 'cash', name: 'Cash' },
    { value: 'cheque', label: 'Cheque', id: 'cheque', name: 'Cheque' },
    { value: 'online', label: 'Online', id: 'online', name: 'Online' },
    { value: 'NACH', label: 'NACH', id: 'NACH', name: 'NACH' },
]

export const recurringType = [
    { value: 'month', label: 'Month', id: 'month', name: 'Month' },
    { value: 'year', label: 'Year', id: 'year', name: 'Year' },
]

export const memberTabs = [
    // { value: 0, label: 'about' },
    { value: 0, label: 'subscription' },
    { value: 1, label: 'docs' },
    { value: 2, label: 'payment' },
    { value: 3, label: 'Add Payment' },
]
export const memberTabPages = [
    // { value: 0, path: 'about', component: null },
    { value: 0, path: 'subscription', component: MemberSubscription },
    { value: 1, path: 'docs', component: MemberDocs },
    { value: 2, path: 'payment', component: MemberPayment },
    { value: 3, path: 'add', component: MemberPaymentAdd },
]

export const timeInterval = [
    { id: "0", name: "This Month" },
    { id: "1", name: "Last Month" },
    { id: "3", name: "3 Month" },
    { id: "6", name: "6 Month" },
    { id: "12", name: "1 Year" },
    { id: "custom", name: "Custom" }
]

export function getDistricts(user) {
    let mDistrict = [];
    if (user !== null && typeof user !== "undefined") {
        let tDsct = JSON.parse(user).permissions;
        for (let i = 0; i < tDsct.length; i++) {
            let mItem = {
                id: tDsct[i],
                name: tDsct[i]
            }
            mDistrict.push(mItem);
        }
    }
    return mDistrict;
}


export const USER_ACCOUNT = "USER_ACCOUNT";
export const SUPER_USER = "SUPER_USER";
export const ADMIN = "ADMIN";
export const FILTER = "FILTER";