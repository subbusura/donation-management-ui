import { errorAlert } from "../common/Common";

export function validateResponse(response) {

    if (response.status === 500) {

        return { message: 'Internal server error', isAuth: true, isValidation: false };
    }
    if (response.status === 401) {
        errorAlert("Unauthorized access");
        localStorage.clear();
        window.location.reload();
        return { message: 'Unauthorized access', isAuth: false, isValidation: false };
    }
    if (response.status === 403) {
        errorAlert("Access Denied");
        return { message: 'Invalid token', isAuth: false, isValidation: false };
    }
    if (response.status === 422) {
        
        return { message: 'Validation error', isAuth: true, isValidation: true };
    }
    if (response.status === 400) {
        errorAlert("Missing parameters");
        return { message: 'Missing parameters', isAuth: true, isValidation: false };
    }
    if (response.status === 405) {
        errorAlert("Bad request");
        return { message: 'Bad request', isAuth: true, isValidation: false };
    }
    if (response.status === 404) {
        //window.alert("Page not found");
        errorAlert("page not found");
        return { message: 'Page not found', isAuth: true, isValidation: false };
    }

}