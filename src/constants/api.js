//export const BASE_URL = process.env.REACT_APP_ENDPOINT;
export const BASE_URL = '/api';
export const BASE_URL_V1 = '/api/v1';
//export const BASE_URL_V1 = process.env.REACT_APP_ENDPOINT_V1;
//export const ADMIN_BASE_URL = process.env.REACT_APP_BASE_URL;

export const LOGIN = '/auth/get/token';
export const LOGOUT_USER = '/user/session/logout';
export const APPLICATION_LIST = '/application';
export const APPLICATION_CREATE = '/application/create';
export const USER_CREATE = '/user';
export const MASTER_COUNTRY = '/master/country';
export const MASTER_STATE = '/master/state';
export const MASTER_CITY = '/master/city';
export const MASTER_AREA = '/master/area';
export const MASTER_BANK = '/master/bank';
export const MASTER_ORGANIZATION = '/master/organization';
export const COUNTRY = '/country';
export const MEMBER = '/member';
export const BANK = '/bank';


export const buildURL = (action, params = {}) => {

    let url = "";
    Object.keys(params).forEach((keys, index) => {

        if (index === 0) {
            url += "?" + keys + "=" + params[keys]
        } else {
            url += "&" + keys + "=" + params[keys]
        }

    })

    if (process.env.NODE_ENV !== "production") {

        const lparams = url === "" ? "?" : url;

        return BASE_URL_V1 + action + lparams;
    }

    return BASE_URL_V1 + action + url;

};

export const buildURL_v1 = (action, params = {}) => {

    let url = "";
    Object.keys(params).forEach((keys, index) => {

        if (index === 0) {
            url += "?" + keys + "=" + params[keys]
        } else {
            url += "&" + keys + "=" + params[keys]
        }

    })

    if (process.env.NODE_ENV !== "production") {

        const lparams = url === "" ? "?" : url;

        return BASE_URL + action + lparams;
    }

    return BASE_URL + action + url;

};

export function logout(){
    window.localStorage.clear();
    window.location = '/';
    window.history.replaceState(null, null, "/");
}


