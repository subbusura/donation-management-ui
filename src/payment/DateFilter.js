import React, { Component } from 'react';
import { DateInput } from 'react-admin';
import { Field } from 'redux-form'

export default class DateFilter extends Component {
    render() {
        const styles = {
            row: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
            },
        };

        return (
            <span style={styles.row} >
                <Field
                    name={this.props.fromSource}
                    label={this.props.fromLabel}
                    component={DateInput}
                />
                &nbsp;
                <Field
                    name={this.props.toSource}
                    label={this.props.toLabel}
                    component={DateInput}
                />
            </span>
        );
    }
}
