import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, Icon, IconButton, FormControl, MenuItem, Select, FormHelperText, InputLabel } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Axios from 'axios';
import { buildURL, MASTER_BANK, MASTER_ORGANIZATION, buildURL_v1, BANK } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import { commonLog, errorAlert, successAlert } from '../common/Common';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { donationType, paymentType } from '../constants/Constants';


class PaymentEntry extends React.Component {

    constructor(props) {
        super(props);
        this.id = this.props.id;
        this.state = {
            payment: {
                organization_id: '',
                bank_name: '',
                scheme_type: '',
                payment_type: '',
                amount: '',
                cheque_number: '',
                cheque_date: '',
                sms: 1,
            },
            error: {
                organization_id: [],
                bank_name: [],
                scheme_type: [],
                payment_type: [],
                amount: [],
                cheque_number: [],
                cheque_date: []
            },
            organizationList: [],
            bankList: [],
            isLoading: false
        }
    }

    componentDidMount() {

        this.getOrganization();
        this.getBankList();
        this.getPayment();
    }

    getPayment() {
        Axios.get(buildURL(`/payment/${this.id}`))
            .then((res) => {
                //commonLog("PRops", res.data.items)
                this.setState({ payment: res.data });
            })
            .catch((error) => {

            });
    }

    getOrganization() {
        Axios.get(buildURL_v1(MASTER_ORGANIZATION))
            .then((res) => {
                //commonLog("PRops", res.data.items)
                this.setState({ organizationList: res.data.items });
            })
            .catch((error) => {

            });
    }

    getBankList() {
        Axios.get(buildURL(BANK))
            .then((res) => {
                //commonLog("PRops", res.data.items)
                this.setState({ bankList: res.data });
            })
            .catch((error) => {

            });
    }

    onHandleChange(e) {
        let mpayment = this.state.payment;
        mpayment[e.target.name] = e.target.value;
        if (e.target.name === 'payment_type') {
            mpayment.cheque_number = "";
        }
        this.setState({ payment: mpayment });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        // let mpayment = {
        //     organization_id: this.state.payment.organization_id,
        //     payment_code: this.state.payment.payment_code,
        //     phone_code: this.state.payment.phone_code,
        // }
        let mpayment = this.state.payment;
        mpayment.amount = parseInt(mpayment.amount);

        Axios.put(buildURL(`/payment/${this.props.id}/receive`), mpayment)
            .then((response) => {
                successAlert("Amount received");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mpayment = this.state.payment;
        Object.keys(mpayment).forEach((key) => {
            mpayment[key] = "";
        });
        this.setState({ payment: mpayment });
    }

    validate() {
        let mpayment = this.state.payment;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mError).forEach((key) => {
            if (key === "cheque_number") {
                if (mpayment.payment_type !== "cash" && mpayment.payment_type !== "NACH" && (mpayment[key] === null || mpayment[key] === "")) {
                    mError[key] = "Reference number Should not be empty";
                    isError = true;
                }else{
                    mError[key] = "";
                }
            } else if (key === "cheque_date") {
                if (mpayment.payment_type === "cheque" && (mpayment[key] === null || mpayment[key] === "")) {
                    mError[key] = "Date Should not be empty";
                    isError = true;
                }else{
                    mError[key] = "";
                }
            } else if (mpayment[key] === "") {
                mError[key] = key + " Should not be empty";
                isError = true;
            } else {
                mError[key] = "";
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteItem() }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });

    }

    deleteItem() {
        Axios.delete(buildURL_v1(MASTER_BANK + "/" + this.props.id))
            .then((response) => {
                successAlert("Deleted successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    render() {

        const { payment, error, organizationList, bankList } = this.state;

        let organization = organizationList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });
        let bank = bankList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let scheme_type = donationType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let payment_type = paymentType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Payment' ></CardHeader>

                            <CardContent>

                                <Grid container spacing={8}>
                                    <Grid item xs={6}>

                                        <FormControl fullWidth error={error.organization_id.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Organization</InputLabel>
                                            <Select
                                                name='organization_id'
                                                fullWidth
                                                value={payment.organization_id}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {organization}</Select>
                                            <FormHelperText>{error.organization_id}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6}>

                                        <FormControl fullWidth error={error.bank_name.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Bank</InputLabel>
                                            <Select
                                                name='bank_name'
                                                fullWidth
                                                value={payment.bank_name}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {bank}</Select>
                                            <FormHelperText>{error.bank_name}</FormHelperText>
                                        </FormControl>

                                    </Grid>

                                    <Grid item xs={6}>
                                        <FormControl fullWidth error={error.scheme_type.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Donation type</InputLabel>
                                            <Select
                                                name='scheme_type'
                                                fullWidth
                                                value={payment.scheme_type}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {scheme_type}</Select>
                                            <FormHelperText>{error.scheme_type}</FormHelperText>
                                        </FormControl>

                                    </Grid>
                                    <Grid item xs={6}>
                                        <FormControl fullWidth error={error.payment_type.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Payment type</InputLabel>
                                            <Select
                                                name='payment_type'
                                                fullWidth
                                                value={payment.payment_type}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {payment_type}</Select>
                                            <FormHelperText>{error.payment_type}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    {payment.payment_type === "cheque" || payment.payment_type === "online" ?
                                        <Grid item xs={6}>
                                            <TextField
                                                name='cheque_number'
                                                label='Reference No'
                                                fullWidth
                                                helperText={error.cheque_number}
                                                value={payment.cheque_number}
                                                error={error.cheque_number.length > 0 ? true : false}
                                                onChange={(e) => this.onHandleChange(e)}
                                            />
                                        </Grid>
                                        : ""
                                    }
                                    {payment.payment_type === "cheque" ?
                                        <Grid item xs={6}>
                                            <TextField
                                                name='cheque_date'
                                                label='Date'
                                                fullWidth
                                                type="date"
                                                helperText={error.cheque_date}
                                                value={payment.cheque_date}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                error={error.cheque_date.length > 0 ? true : false}
                                                onChange={(e) => this.onHandleChange(e)}
                                            />
                                        </Grid>
                                        : ""
                                    }
                                    <Grid item xs={6}>
                                        <TextField
                                            name='amount'
                                            label='Amount'
                                            fullWidth
                                            type="number"
                                            disabled
                                            helperText={error.amount}
                                            value={payment.amount}
                                            error={error.amount.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                    </Grid>
                                </Grid>


                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Receive</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default PaymentEntry;