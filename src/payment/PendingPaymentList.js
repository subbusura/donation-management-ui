import React from 'react';
import { List, Datagrid, TextField, ReferenceInput, SelectInput, Pagination, TextInput, Filter, DateField, downloadCSV } from 'react-admin';
import { Button } from '@material-ui/core';
import CustomInput from './CustomInput';
import DateFilter from './DateFilter';
import { getDistricts, SUPER_USER, USER_ACCOUNT, ADMIN } from '../constants/Constants';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="name" alwaysOn />
        {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) ?
            <ReferenceInput label="District" source="city" reference="master/city" allowEmpty={false} perPage={500} >
                <SelectInput optionText="name" source="name" optionValue="name"
                    FormHelperTextProps={{ classes: { root: 'hide-helper' } }}
                />
            </ReferenceInput>
            :
            <SelectInput
                label="District"
                source="city"
                choices={getDistricts(localStorage.getItem(USER_ACCOUNT))}
                allowEmpty={false}
            />
        }
        <CustomInput
            source="payment_range"
            fromLabel="From"
            toLabel="To"
            fromSource="payment_range.from"
            toSource="payment_range.to"
            label="Payment Range"
        />
        <DateFilter
            source="entry_date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="entry_date_range.from"
            toSource="entry_date_range.to"
            label="Entry Time"
        />
    </Filter>
);

const exporter = list => {
    const exportableMember = list.map((item) => {
        item.member = item.member.name;
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['member', 'amount', 'entry_datetime']
    });
    downloadCSV(csv, 'Pending Payments');
}

const PendingPaymentList = props => (

    <List {...props}
        bulkActionButtons={false}
        filters={<PostFilter />}
        exporter={exporter}
        pagination={<Pagination perPage={100} rowsPerPageOptions={[50,100,200]}/>}
        perPage={100}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField label="Member" source="member.name" />
            <TextField source="amount" />
            <TextField source="entry_datetime" />
        </Datagrid>
    </List>
);

export default PendingPaymentList;
