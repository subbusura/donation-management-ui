import React from 'react';
import { Grid, Button, Table, Paper, TextField, TableBody, TableHead, TableRow, TableCell } from '@material-ui/core';
import Axios from 'axios';
import { buildURL, MASTER_BANK, MASTER_ORGANIZATION, buildURL_v1, BANK, MEMBER } from '../constants/api';
import { validateResponse } from '../constants/ResponseValidation';
import { Link } from 'react-router-dom';

class PaymentMember extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            memberList: []
        }
    }

    componentDidMount() {
        this.getMemberList()
    }

    getMemberList() {
        Axios.all([
            Axios.get(buildURL(MEMBER, { 'per-page': 0 }))
        ])
            .then(Axios.spread((member) => {
                this.setState({ memberList: member.data.items });
            }))
            .catch((error) => {
                validateResponse(error.response);
            });
    }

    onHandleSearch(e){
        Axios.all([
            Axios.get(buildURL(MEMBER, {filter:JSON.stringify({q:e.target.value}), 'per-page':0}),)
        ])
            .then(Axios.spread((member) => {
                this.setState({ memberList: member.data.items });
            }))
            .catch((error) => {
                validateResponse(error.response);
            });
    }

    render() {
        return (
            <div>
                <Paper square elevation={1} style={{ marginTop: '16px', padding: '8px' }} >
                    <Grid item xs={3} style={{margin:'16px'}} >
                        <TextField
                            name='cheque_number'
                            label='Search'
                            fullWidth
                            onChange={(e) => this.onHandleSearch(e)}
                        />
                    </Grid>
                    {this.renderTable()}
                </Paper>
            </div>
        )
    }

    renderTable() {

        let mItems = this.state.memberList.map((item, index) => {
            return (
                <TableRow>
                    <TableCell>{item.name}</TableCell>
                    <TableCell>{item.email}</TableCell>
                    <TableCell>{item.mobile_number}</TableCell>
                    <TableCell>{item.whatsapp_number}</TableCell>
                    <TableCell>{item.city}</TableCell>
                    <TableCell>{item.state}</TableCell>
                    <TableCell>{item.country}</TableCell>
                    <TableCell>
                        <Link to={`/payment/add/${item.id}`} >Add</Link>
                    </TableCell>

                </TableRow>
            )
        })

        return (
            <Grid container spacing={16} style={{ marginTop: '32px' }} >

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Name</TableCell>
                            <TableCell align="right">Email</TableCell>
                            <TableCell align="right">Mobile Number</TableCell>
                            <TableCell align="right">Whatsapp Number</TableCell>
                            <TableCell align="right">City</TableCell>
                            <TableCell align="right">State</TableCell>
                            <TableCell align="right">Country</TableCell>
                            <TableCell align="right">Payment</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {mItems}
                    </TableBody>
                </Table>


            </Grid>
        )
    }

}

export default PaymentMember;