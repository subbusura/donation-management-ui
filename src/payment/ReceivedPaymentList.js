import React from 'react';
import { List, Datagrid, TextField, ReferenceInput, TextInput, Filter, DeleteButton, Pagination, SelectInput, UrlField, FileField, downloadCSV } from 'react-admin';
import { Button } from '@material-ui/core';
import CustomInput from './CustomInput';
import DateFilter from './DateFilter';
import { paymentType, recurringType, donationType, SUPER_USER, getDistricts, USER_ACCOUNT, ADMIN } from '../constants/Constants';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="name" alwaysOn />
        <ReferenceInput label="Organization" source="organization_id" reference="master/organization" allowEmpty={false}>
            <SelectInput optionText="name" source="name" />
        </ReferenceInput>
        <SelectInput
            source="scheme_type"
            choices={donationType}
            allowEmpty={false}
        >
        </SelectInput>
        <SelectInput
            source="payment_type"
            choices={paymentType}
            allowEmpty={false}
        />
        <ReferenceInput key={"name"} label="Bank" source="bank_name" reference="master/bank" allowEmpty={false} >
            <SelectInput optionText="name" source="name" optionValue="name"
                FormHelperTextProps={{ classes: { root: 'hide-helper' } }}
            />
        </ReferenceInput>
        <CustomInput
            source="payment_range"
            fromLabel="From"
            toLabel="To"
            fromSource="payment_range.from"
            toSource="payment_range.to"
            label="Payment Range"
        />
        {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) ?
            <ReferenceInput label="District" source="city" reference="master/city" allowEmpty={false} perPage={500} >
                <SelectInput optionText="name" source="name" optionValue="name"
                    FormHelperTextProps={{ classes: { root: 'hide-helper' } }}
                />
            </ReferenceInput>
            :
            <SelectInput
                label="District"
                source="city"
                choices={getDistricts(localStorage.getItem(USER_ACCOUNT))}
                allowEmpty={false}
            />
        }
        <DateFilter
            source="entry_date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="entry_date_range.from"
            toSource="entry_date_range.to"
            label="Entry Time"
        />
        <DateFilter
            source="received_date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="received_date_range.from"
            toSource="received_date_range.to"
            label="Received Time"
        />
    </Filter>
);

const exporter = list => {
    const exportableMember = list.map((item) => {
        item.member = item.member.name;
        item.organization = item.organization.name;
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['member', 'organization', 'scheme_type', 'payment_type', 'bank_name', 'amount', 'entry_datetime', 'received_datetime']
    });
    downloadCSV(csv, 'Received Payments');
}

const ReceivedPaymentList = props => (

    <List {...props}
        bulkActionButtons={false}
        filters={<PostFilter />}
        exporter={exporter}
        pagination={<Pagination perPage={500} rowsPerPageOptions={[50,100,200]}/>}
        perPage={100}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField label="Member" source="member.name" />
            <TextField label="Organization" source="organization.name" />
            <TextField source="scheme_type" />
            <TextField source="payment_type" />
            <TextField source="bank_name" />
            <TextField source="amount" />
            <FileField label="Receipt" value="test" title="Receipt" target="_blank " source="receipt_location" />
            <TextField source="entry_datetime" />
            <TextField source="received_datetime" />
            <DeleteButton/>
        </Datagrid>
    </List>
);

export default ReceivedPaymentList;
