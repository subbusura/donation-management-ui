import React, { Component } from 'react';
import { TextInput, DateInput } from 'react-admin';
import { Field } from 'redux-form'

export default class CustomInput extends Component {
    render() {
        const styles = {
            row: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
            },
        };

        console.log("Input props", this.props);

        return (
            <span style={styles.row} source={"payment_range"} >
                <Field
                    name={this.props.fromSource}
                    label={this.props.fromLabel}
                    component={TextInput}
                />
                &nbsp;
                <Field
                    name={this.props.toSource}
                    label={this.props.toLabel}
                    component={TextInput}
                />
            </span>
        );
    }
}
