import React from 'react';
// import { Admin } from 'react-admin';
// import { Admin, Resource, ListGuesser } from 'react-admin';
// import { Admin, Resource } from 'react-admin';
// import { Admin, Resource, ListGuesser } from 'react-admin';

import { fetchUtils, Admin, Resource, EditGuesser, Login } from 'react-admin';
import UserIcon from '@material-ui/icons/Group';
import  UserList from './user/UserList';
//import jsonServerProvider from 'ra-data-json-server';
import simpleRestProvider from 'ra-data-simple-rest';
import Dashboard from './dashboard/Dashboard';
import authProvider from './authProvider';
import { commonLog } from './common/Common';
import ApplicationCreate from './application/ApplicationCreate';
import Axios from 'axios';
import DataProvider from './dataprovider'
import UserCreate from './user/UserCreate';
import CustomRoutes from './CustomRoutes';
import Menu from './menu/Menu';
import CountryList from './master/country/CountryList';
import CountryCreate from './master/country/CountryCreate';
import CountryUpdate from './master/country/CountryUpdate';

import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import 'react-s-alert/dist/s-alert-css-effects/scale.css';
import 'react-s-alert/dist/s-alert-css-effects/bouncyflip.css';
import 'react-s-alert/dist/s-alert-css-effects/flip.css';
import 'react-s-alert/dist/s-alert-css-effects/genie.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import StateList from './master/state/StateList';
import StateCreate from './master/state/StateCreate';
import StateUpdate from './master/state/StateUpdate';
import CityList from './master/city/CityList';
import CityCreate from './master/city/CityCreate';
import CityUpdate from './master/city/CityUpdate';
import BankUpdate from './master/bank/BankUpdate';
import BankCreate from './master/bank/BankCreate';
import BankList from './master/bank/BankList';
import MemberCreate from './member/MemberCreate';
import MemberUpdate from './member/MemberUpdate';
import MemberList from './member/MemberList';
import MemberView from './member/MemberView';
import PendingPaymentList from './payment/PendingPaymentList';
import PaymentEntry from './payment/PaymentEntry';
import OrganizationList from './master/organization/OrganizationList';
import OrganizationCreate from './master/organization/OrganizationCreate';
import OrganizationUpdate from './master/organization/OrganizationUpdate';
import AreaList from './master/area/AreaList';
import AreaCreate from './master/area/AreaCreate';
import AreaUpdate from './master/area/AreaUpdate';
import Settings from './settings/Settings';
import ReceivedPaymentList from './payment/ReceivedPaymentList';
import UserUpdate from './user/UserUpdate';
import PaymentAdd from './payment/PaymentAdd';
import SubscriptionList from './subscription/SubscriptionList';
import { darkTheme, lightTheme } from './layout/themes';
import SmsTemplate from "./settings/SmsTemplate";

//const token = "";

//if (JSON.parse(localStorage.getItem('user')) !== null) {
const token = localStorage.getItem('user');
//}


Axios.defaults.headers.common['Authorization'] = `${token}`;

const MyLoginPage = () => <Login backgroundImage="./images/login_bg.jpeg" />;

// const App = () => <Admin dataProvider={dataProvider} />;
const App = () => (
  <Admin dashboard={Dashboard} loginPage={MyLoginPage} theme={lightTheme} authProvider={authProvider} menu={Menu} customRoutes={CustomRoutes} dataProvider={DataProvider}>

    <Resource name="user" list={UserList} create={UserCreate} icon={UserIcon} />
    <Resource options={{ label: 'Country' }} name="master/country" list={CountryList} create={CountryCreate} edit={CountryUpdate} />
    <Resource options={{ label: 'State' }} name="master/state" list={StateList} create={StateCreate} edit={StateUpdate} />
    <Resource options={{ label: 'City' }} name="master/city" list={CityList} create={CityCreate} edit={CityUpdate} />
    <Resource options={{ label: 'Area' }} name="master/area" list={AreaList} create={AreaCreate} edit={AreaUpdate} />
    <Resource options={{ label: 'Bank' }} name="master/bank" list={BankList} create={BankCreate} edit={BankUpdate} />
    <Resource options={{ label: 'Organization' }} name="master/organization" list={OrganizationList} create={OrganizationCreate} edit={OrganizationUpdate} />
    <Resource options={{ label: 'Member' }} name="member" list={MemberList} create={MemberCreate} show={MemberView} edit={MemberUpdate} />
    <Resource options={{ label: 'Pending Payment' }} name="payment/pending" list={PendingPaymentList} edit={PaymentEntry} />
    <Resource options={{ label: 'Received Payment' }} name="payment" list={ReceivedPaymentList}/>
    <Resource options={{ label: 'Payment Add' }} name="add" list={PaymentAdd}/>
    <Resource options={{ label: 'Subscription' }} name="recurring" list={SubscriptionList}/>
    <Resource options={{ label: 'Settings' }} name="setting/sms/template" list={SmsTemplate}/>
    <Alert stack={{ limit: 3 }} />
  </Admin>
);

export default App;
