import React,{Component} from 'react';
import MainLayout from './../../layouts/MainLayout';
import LoginLayout from './../../layouts/LoginLayout';


  const Layout=(WrappedComponent,layout)=>{

         return class Layout extends Component{
            
             render(){
                let component=null;
                  switch(layout)
                  {
                      case "main":
                      component=<MainLayout {...this.props}><WrappedComponent  /></MainLayout>
                      break;
                      case "login":
                      component=<LoginLayout {...this.props}><WrappedComponent  /></LoginLayout>
                      break;
                      default:
                      component =<div>Layout Not Found</div>
                  }

                return (<div>
                            {
                                component
                            }
                </div>)

             }


         }


  }


  export default Layout;