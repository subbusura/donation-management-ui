import React,{Component} from 'react';

  const AuthPageHoc=(WrappedComponent)=>{

         return class AuthPageHoc extends Component{
            
            isToken(){

                if (typeof localStorage !== 'undefined') {
                    let x = localStorage.getItem('access_key');
                    if(x===null || x==='')
                    {
                        return false;
                        
                    }else{

                        return true;
                    }

                } else {
                    return false;
                }

            }

             render(){

                return this.isToken()?<WrappedComponent  {...this.props} can_access={true}/>:<div>Your Are Not Authorized To Access the Page Please Login</div>

             }


         }
  }
  export default AuthPageHoc;