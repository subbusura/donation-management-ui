import React from 'react';
import {
    Create,
    FormTab,
    NumberInput,
    ReferenceInput,
    SelectInput,
    TabbedForm,
    DateInput,
    TextInput,
    required,
    SimpleForm
} from 'react-admin';
import withStyles from '@material-ui/core/styles/withStyles';
import { Typography } from '@material-ui/core';
//import RichTextInput from 'ra-input-rich-text';

export const styles = {
    stock: { width: '5em' },
    price: { width: '5em' },
    width: { width: '5em' },
    widthFormGroup: { display: 'inline-block' },
    height: { width: '5em' },
    heightFormGroup: { display: 'inline-block', marginLeft: 32 },
};

export class ProfileCreate extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Create  {...this.props}>

                <SimpleForm  >

                    <Typography variant="title">Create Profile</Typography>

                    <TextInput
                        source="First name"
                    />
                    <TextInput
                        source="Last name"
                    />
                </SimpleForm>


            </Create>
        )

    }

}
export default withStyles(styles)(ProfileCreate);
