import ProfileList from './ProfileList';
import ProfileCreate from './ProfileCreate';
import ProfileEdit from './ProfileEdit';

export default {
    list: ProfileCreate,
    create: ProfileCreate,
    edit: ProfileEdit,
};