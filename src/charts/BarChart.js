import React, { Component } from 'react';
import CanvasJSReact from '../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class BarChart extends Component {

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			title: {
				type: '',
				date_range: ''
			}
		}
	}

	componentDidMount() {
		this.setState({ data: this.props.data, title: this.props.title });
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ data: nextProps.data, title: nextProps.title });
	}

	render() {
		let { title } = this.state;
		const options = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: "Total " + title.type + " amount ( "+title.date_range.from +" - "+title.date_range.to+" )",
				fontSize: 20,
				padding: 20
			},
			data: [{
				type: "column",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: {y}",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 16,
				dataPoints: this.state.data
			}]
		}

		return (
			<div>

				<CanvasJSChart options={options}
				/* onRef={ref => this.chart = ref} */
				/>
				{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
			</div>
		);
	}
}

export default BarChart;