import React from 'react';
import { Route } from 'react-router-dom';
import ApplicationView from './application/ApplicationView';
import CountryUpdate from './master/country/CountryUpdate';
import MemberUpdate from './member/MemberUpdate';
import MemberView from './member/MemberView';
import MemberCreate from './member/MemberCreate';
import Settings from './settings/Settings';
import UserUpdate from './user/UserUpdate';
import UserCreate from './user/UserCreate';
import PaymentAdd from './payment/PaymentAdd';
import Memberimport from "./member/MemberImport";
import PaymentMember from './payment/PaymentMember';


export default [
    <Route exact path="/member/create" component={MemberCreate} />,
    <Route exact path="/member/import" component={Memberimport} />,
    <Route exact path="/member/:id" component={MemberView} />,
    <Route exact path="/member/:id/update" component={MemberUpdate} />,
    <Route exact path="/member/:id/:path" component={MemberView} />,
    <Route exact path="/settings" component={Settings} />,
    <Route exact path="/user/create" component={UserCreate} />,
    <Route exact path="/user/:id" component={UserUpdate} />,
    <Route exact path="/payment/add" component={PaymentAdd} />,    
    // <Route exact path="/master/country/:id" component={CountryUpdate} />,

];