import React from 'react';
import { List, Datagrid, TextField, EditButton, Pagination, Filter, TextInput, downloadCSV} from 'react-admin';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const ListFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

const exporter = list => {
    const exportableMember = list.map((item) => {
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['name']
    });
    downloadCSV(csv, 'Bank');
}

const BankList = props => (

    <List filters={<ListFilter />} {...props} exporter={exporter}  perPage={25} pagination={<Pagination perPage={500} rowsPerPageOptions={[10,25,50]}/>}
        bulkActionButtons={false}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField source="name" />
            <EditButton />
        </Datagrid>
    </List>
);

export default BankList;
