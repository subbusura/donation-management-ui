import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, MASTER_STATE, buildURL_v1, MASTER_CITY, COUNTRY } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../../common/Common';
import Select from 'react-select';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';


class CityUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            state: {
                name: '',
                state_id: '',
            },
            error: {
                name: [],
                state_id: [],
            },
            isLoading: false,
            country: [],
            states: [],
            selectedValueCountry: {},
            selectedValueState: {},
            selectedValue: null,
        }
    }

    componentDidMount() {
        //this.fetchStateList();
        //        this.fetchState();
        this.fetchCity();
    }

    fetchCity() {
        Axios.get(buildURL_v1(MASTER_CITY + "/" + this.props.id))
            .then((res) => {
                let mValue = { value: res.data.state_id, label: res.data.state_id };
                let selectedValueCountry = { value: res.data.country_code, label: res.data.country }
                let selectedValueState = { value: res.data.state_id, label: res.data.state };
                let country = [selectedValueCountry]
                let states = [selectedValueState]
                this.setState({
                    state: res.data,
                    selectedValue: mValue,
                    selectedValueCountry: selectedValueCountry,
                    selectedValueState: selectedValueState,
                    country: country,
                    states: states
                });
                this.fetchStateList(res.data.country_code)
                this.fetchCountryList()
            })
            .catch((error) => {

            });
    }

    fetchStateList(country_code) {

        Axios.get(buildURL_v1("/v1" + COUNTRY + "/" + country_code + "/state", { "per-page": 100 }))
            .then((res) => {
                this.setStateList(res.data);
            })
            .catch((error) => {

            });
    }

    fetchCountryList() {

        Axios.get(buildURL_v1("/v1" + COUNTRY, { "per-page": 100 }))
            .then((res) => {
                this.setCountryList(res.data)
            })
            .catch((error) => {

            });
    }

    setStateList(data) {
        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.id, label: item.name });
        });
        this.setState({ states: mCountry });
    }

    setCountryList(data) {

        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.country_code, label: item.name });
        });
        this.setState({ country: mCountry });
    }

    onHandleChange(e) {
        let mState = this.state.state;
        mState[e.target.name] = e.target.value;
        this.setState({ State: mState });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        let payload = {
            state_id: this.state.selectedValueState.value,
            name: this.state.state.name
        }

        Axios.put(buildURL_v1(MASTER_CITY + "/" + this.props.id), payload)
            .then((response) => {
                successAlert("Updated successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mstate = this.state.state;
        Object.keys(mstate).forEach((key) => {
            mstate[key] = "";
        });
        this.setState({ state: mstate });
    }

    validate() {
        let mstate = this.state.state;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mstate).forEach((key) => {
            if (mstate[key] === "") {
                mError[key].push(key + " Should not be empty");
                isError = true;
                if (key === "state_id") {
                    errorAlert("State cannot be empty");
                }
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onCountryChange(value) {
        if (this.state.selectedValueCountry.value != value.value) {
            this.fetchStateList(value.value);
            this.setState({ states: [], state: { state_id: '' }, selectedValueState: {}, selectedValueCountry: value });
        }

    }

    onStateChange(value) {
        let mState = this.state.selectedValueState;
        mState.state_id = value.value;
        this.setState({ state: mState, selectedValueState: value });
    }

    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteItem() }
                },
                {
                    label: 'No',
                    // onClick: () => { onclose() }
                }
            ]
        });

    }

    deleteItem() {
        Axios.delete(buildURL_v1(MASTER_CITY + "/" + this.props.id))
            .then((response) => {
                successAlert("Deleted successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    render() {

        const { state, error, selectedValue, selectedValueState, selectedValueCountry } = this.state;
        commonLog(this.state)

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Update city'
                                action={
                                    <IconButton onClick={(e) => this.onDelete(e)} color="#f44336" >
                                        <DeleteIcon />
                                    </IconButton >
                                }
                            ></CardHeader>

                            <CardContent>

                                <div style={{ marginBottom: '24px' }}>
                                    <Select
                                        name='country_code'
                                        placeholder='Country code'
                                        fullWidth
                                        options={this.state.country}
                                        helperText={error.state_code}
                                        value={selectedValueCountry}
                                        required
                                        onChange={(value) => this.onCountryChange(value)}
                                        style={{ marginTop: '32px' }}
                                    />
                                </div>

                                <div style={{ marginBottom: '16px' }}>

                                    <Select
                                        name='state_id'
                                        placeholder='State'
                                        fullWidth
                                        options={this.state.states}
                                        helperText={error.state_code}
                                        value={selectedValueState}
                                        required
                                        onChange={(value) => this.onStateChange(value)}
                                        style={{ marginTop: '32px' }}
                                    />

                                </div>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={state.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginBottom: '16px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Update</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default CityUpdate;