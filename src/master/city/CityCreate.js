import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, buildURL_v1, MASTER_CITY, MASTER_STATE, COUNTRY } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../../common/Common';
import Select from 'react-select';

const selectItem = {
    marginBottom: '32px'
}

class CityCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            state: {
                name: '',
                state_id: '',
            },
            error: {
                name: [],
                state_id: [],
            },
            isLoading: false,
            country: [],
            stateList: [],
            selectedValue: null,
            selectedCountry: null,
        }
    }

    componentDidMount() {
        Axios.get(buildURL(COUNTRY, { "per-page": 100 }))
            .then((res) => {
                this.setCountryList(res.data);
            })
            .catch((error) => {

            });
    }

    setCountryList(data) {
        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.country_code, label: item.name });
        });
        this.setState({ country: mCountry });
    }

    setStateList(data) {
        let mState = [];
        data.forEach((item, index) => {
            mState.push({ value: item.id, label: item.name });
        });
        this.setState({ stateList: mState });
    }


    onHandleChange(e) {
        let mState = this.state.state;
        mState[e.target.name] = e.target.value;
        this.setState({ State: mState });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        Axios.post(buildURL_v1(MASTER_CITY), this.state.state)
            .then((response) => {
                successAlert("City created successfully");
                this.onSuccess();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mstate = this.state.state;
        mstate.name = "";
        this.setState({ state: mstate });
    }

    validate() {
        let mstate = this.state.state;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mstate).forEach((key) => {
            // if (key === "state_id") {
            //     errorAlert("State cannot be empty");
            // }
            if (mstate[key] === "") {
                mError[key].push(key + " Should not be empty");
                if (key === "state_id") {
                    errorAlert("State cannot be empty");
                }
                isError = true;
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onCountryChange(value) {
        Axios.get(buildURL(`${COUNTRY}/${value.value}/state`, { "per-page": 100 }))
            .then((res) => {
                this.setStateList(res.data);
            })
            .catch((error) => {

            });
        this.setState({ selectedCountry: value });
    }

    onStateChange(value) {
        let mState = this.state.state;
        mState.state_id = value.value;
        this.setState({ State: mState, selectedValue: value });
    }

    render() {

        const { state, error, selectedValue, selectedCountry } = this.state;

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Create city'></CardHeader>

                            <CardContent>

                                <div style={{ marginBottom: '24px' }}>
                                    <Select
                                        name='country_code'
                                        placeholder='Country'
                                        fullWidth
                                        options={this.state.country}
                                        value={selectedCountry}
                                        required
                                        onChange={(value) => this.onCountryChange(value)}
                                    />
                                </div>
                                <div style={{ marginBottom: '8px' }}>
                                    <Select
                                        name='state_id'
                                        placeholder='State'
                                        fullWidth
                                        options={this.state.stateList}
                                        helperText={error.state_code}
                                        value={selectedValue}
                                        error={error.state_id.length > 0 ? true : false}
                                        required
                                        onChange={(value) => this.onStateChange(value)}
                                    />
                                </div>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={state.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginBottom: '16px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Create</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default CityCreate;