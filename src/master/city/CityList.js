import React from 'react';
import { List, Datagrid, TextField, EditButton, Pagination, Filter, TextInput, downloadCSV } from 'react-admin';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const ListFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <TextInput label="Country" source="country" />
        <TextInput label="State" source="state" />
    </Filter>
);

const customPagination = props => <Pagination perPage={500} rowsPerPageOptions={[10,25,50]} {...props} />

const exporter = list => {
    const exportableMember = list.map((item) => {
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['name', 'state', 'country_name']
    });
    downloadCSV(csv, 'City');
}

const CityList = props => (

    <List filters={<ListFilter />} {...props} exporter={exporter}  perPage={25} pagination={<Pagination perPage={500} rowsPerPageOptions={[10,25,50]}/>}
        bulkActionButtons={false}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField source="name" />
            <TextField source="state" />
            <TextField source="country" />
            {/* <TextField source="country_code" /> */}
            <EditButton />
        </Datagrid>
    </List>
);

export default CityList;
