import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, MASTER_STATE, buildURL_v1, MASTER_CITY, COUNTRY, MASTER_AREA } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../../common/Common';
import Select from 'react-select';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';


class AreaUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            state: {
                name: '',
                city_id: ''
            },
            error: {
                name: [],
                city_id: []
            },
            isLoading: false,
            country: [],
            states: [],
            cityList: [],
            selectedValueCountry: {},
            selectedValueState: {},
            selectedValue: null,
        }
    }

    componentDidMount() {
        //this.fetchStateList();
        //        this.fetchState();
        this.fetchCity();
    }

    fetchCity() {
        Axios.get(buildURL_v1(MASTER_AREA + "/" + this.props.id))
            .then((res) => {
                let mValue = { value: res.data.state_id, label: res.data.state_id };
                let selectedValueCountry = { value: res.data.country_code, label: res.data.country }
                let selectedValueState = { value: res.data.state_id, label: res.data.state };
                let selectedValueCity = { value: res.data.city_id, label: res.data.city };
                let country = [selectedValueCountry]
                let states = [selectedValueState]
                this.setState({
                    state: res.data,
                    selectedValue: mValue,
                    selectedValueCountry: selectedValueCountry,
                    selectedValueState: selectedValueState,
                    selectedValueCity: selectedValueCity,
                    country: country,
                    states: states
                });
                this.fetchCityList(res.data.country_code, res.data.state_id);
                this.fetchStateList(res.data.country_code);
                this.fetchCountryList();
            })
            .catch((error) => {

            });
    }

    fetchCityList(country_code, stateId) {

        Axios.get(buildURL(`${COUNTRY}/${country_code}/state/${stateId}/city`, { "per-page": 100 }))
            .then((res) => {
                this.setCityList(res.data);
            })
            .catch((error) => {

            });
    }

    fetchStateList(country_code) {

        Axios.get(buildURL_v1("/v1" + COUNTRY + "/" + country_code + "/state", { "per-page": 100 }))
            .then((res) => {
                this.setStateList(res.data);
            })
            .catch((error) => {

            });
    }

    fetchCountryList() {

        Axios.get(buildURL_v1("/v1" + COUNTRY, { "per-page": 100 }))
            .then((res) => {
                this.setCountryList(res.data)
            })
            .catch((error) => {

            });
    }

    setCityList(data) {
        let mCity = [];
        data.forEach((item, index) => {
            mCity.push({ value: item.id, label: item.name });
        });
        this.setState({ cityList: mCity });
    }

    setStateList(data) {
        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.id, label: item.name });
        });
        this.setState({ states: mCountry });
    }

    setCountryList(data) {

        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.country_code, label: item.name });
        });
        this.setState({ country: mCountry });
    }

    onHandleChange(e) {
        let mState = this.state.state;
        mState[e.target.name] = e.target.value;
        this.setState({ State: mState });
    };

    onSubmit(e) {
        e.preventDefault();

        let payload = {
            city_id: this.state.selectedValueCity.value,
            name: this.state.state.name
        }

        if (this.validate()) {
            return false;
        }

        Axios.put(buildURL_v1(MASTER_AREA + "/" + this.props.id), payload)
            .then((response) => {
                successAlert("Updated successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mstate = this.state.state;
        Object.keys(mstate).forEach((key) => {
            mstate[key] = "";
        });
        this.setState({ state: mstate });
    }

    validate() {
        let mstate = this.state.state;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mstate).forEach((key) => {
            if (mstate[key] === "") {
                mError[key].push(key + " Should not be empty");
                isError = true;
                if (key === "city_id") {
                    errorAlert("City cannot be empty");
                }
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onCountryChange(value) {
        if (this.state.selectedValueCountry.value != value.value) {
            this.fetchStateList(value.value);
            this.setState({ states: [], selectedValueState: {}, selectedValueCountry: value });
        }

    }

    onStateChange(value) {
        if (this.state.selectedValueCountry.value != value.value) {
            this.fetchCityList(this.state.selectedValueCountry.value, value.value);
            this.setState({ cityList: [], state: { city_id: '' }, selectedValueCity: {}, selectedValueState: value });
        }

    }

    onCityChange(value) {
        let mState = this.state.selectedValueCity;
        mState.city_id = value.value;
        this.setState({ state: mState, selectedValueCity: value });
    }


    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteItem() }
                },
                {
                    label: 'No',
                    // onClick: () => { onclose() }
                }
            ]
        });

    }

    deleteItem() {
        Axios.delete(buildURL_v1(MASTER_AREA + "/" + this.props.id))
            .then((response) => {
                successAlert("Deleted successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    render() {

        const { state, error, selectedValue, selectedValueState, selectedValueCountry, selectedValueCity } = this.state;
        commonLog(this.state)

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Update Area'
                                action={
                                    <IconButton onClick={(e) => this.onDelete(e)} color="#f44336" >
                                        <DeleteIcon />
                                    </IconButton >
                                }
                            ></CardHeader>

                            <CardContent>

                                <div style={{ marginBottom: '24px' }}>
                                    <Select
                                        name='country_code'
                                        placeholder='Country code'
                                        fullWidth
                                        options={this.state.country}
                                        helperText={error.state_code}
                                        value={selectedValueCountry}
                                        required
                                        onChange={(value) => this.onCountryChange(value)}
                                        style={{ marginTop: '32px' }}
                                    />
                                </div>

                                <div style={{ marginBottom: '24px' }}>

                                    <Select
                                        name='state_id'
                                        placeholder='State'
                                        fullWidth
                                        options={this.state.states}
                                        helperText={error.state_code}
                                        value={selectedValueState}
                                        required
                                        onChange={(value) => this.onStateChange(value)}
                                        style={{ marginTop: '32px' }}
                                    />

                                </div>

                                <div style={{ marginBottom: '16px' }}>

                                    <Select
                                        name='city_id'
                                        placeholder='City'
                                        fullWidth
                                        options={this.state.cityList}
                                        helperText={error.city_id}
                                        value={selectedValueCity}
                                        error={error.city_id.length > 0 ? true : false}
                                        required
                                        onChange={(value) => this.onCityChange(value)}
                                        style={{ marginTop: '32px' }}
                                    />

                                </div>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={state.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginBottom: '16px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Update</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default AreaUpdate;