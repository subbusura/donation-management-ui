import React from 'react';
import { List, Datagrid, TextField, EditButton, Pagination, Filter, TextInput, downloadCSV } from 'react-admin';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const ListFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <TextInput label="Country" source="country" />
        <TextInput label="State" source="state" />
    </Filter>
);

const exporter = list => {
    const exportableMember = list.map((item) => {
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['name','city', 'state', 'country']
    });
    downloadCSV(csv, 'Area');
}

const AreaList = props => (

    <List filters={<ListFilter />} {...props} exporter={exporter}  perPage={25} pagination={<Pagination rowsPerPageOptions={[10,25,50]}/>}
        bulkActionButtons={false}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField source="name" />
            <TextField source="city" />
            <TextField source="state" />
            <TextField source="country" />
            {/* <TextField source="country_code" /> */}
            <EditButton />
        </Datagrid>
    </List>
);

export default AreaList;
