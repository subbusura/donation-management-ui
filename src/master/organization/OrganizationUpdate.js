import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, Icon, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Axios from 'axios';
import { buildURL, MASTER_BANK, buildURL_v1, MASTER_ORGANIZATION } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { commonLog, errorAlert, successAlert } from '../../common/Common';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';


class OrganizationUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            country: {
                name: '',
                // country_code: '',
                // phone_code: '',
            },
            error: {
                name: [],
                // country_code: [],
                // phone_code: [],
            },
            isLoading: false
        }
    }

    componentDidMount() {

        Axios.get(buildURL_v1(MASTER_ORGANIZATION + "/" + this.props.id))
            .then((res) => {
                commonLog("PRops", res)
                this.setState({ country: res.data });
            })
            .catch((error) => {

            });
    }

    onHandleChange(e) {
        let mCountry = this.state.country;
        mCountry[e.target.name] = e.target.value;
        this.setState({ country: mCountry });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        let mCountry = {
            name: this.state.country.name,
            country_code: this.state.country.country_code,
            phone_code: this.state.country.phone_code,
        }

        Axios.put(buildURL_v1(MASTER_ORGANIZATION + "/" + this.props.id), mCountry)
            .then((response) => {
                successAlert("Organization updated successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mCountry = this.state.country;
        Object.keys(mCountry).forEach((key) => {
            mCountry[key] = "";
        });
        this.setState({ country: mCountry });
    }

    validate() {
        let mCountry = this.state.country;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mCountry).forEach((key) => {
            if (mCountry[key] === "") {
                mError[key] = key + " Should not be empty";
                isError = true;
            } else {
                mError[key] = "";
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteItem() }
                },
                {
                    label: 'No',
                    onClick: () => {}
                }
            ]
        });

    }

    deleteItem() {
        Axios.delete(buildURL_v1(MASTER_ORGANIZATION + "/" + this.props.id))
            .then((response) => {
                successAlert("Deleted successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    render() {

        const { country, error } = this.state;

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Update Organization'
                                action={
                                    <IconButton onClick={(e) => this.onDelete(e)} color="#f44336" >
                                        <DeleteIcon />
                                    </IconButton >
                                }
                            ></CardHeader>

                            <CardContent>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={country.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                />
                                {/* <TextField
                                    name='country_code'
                                    label='Country Code'
                                    fullWidth
                                    helperText={error.country_code}
                                    value={country.country_code}
                                    error={error.country_code.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />
                                <TextField
                                    required
                                    name='phone_code'
                                    label='Phone Code'
                                    fullWidth
                                    error={error.phone_code.length > 0 ? true : false}
                                    helperText={error.phone_code}
                                    value={country.phone_code}
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                /> */}

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Update</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default OrganizationUpdate;