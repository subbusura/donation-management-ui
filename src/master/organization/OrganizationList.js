import React from 'react';
import { List, Datagrid, TextField,Pagination, EditButton, Filter, TextInput } from 'react-admin';

const ListFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

const OrganizationList = props => (

    <List filters={<ListFilter />} {...props} perPage={25} pagination={<Pagination perPage={500} rowsPerPageOptions={[10,25,50]}/>}
        bulkActionButtons={false}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField source="name" />
            <EditButton />
        </Datagrid>
    </List>
);

export default OrganizationList;
