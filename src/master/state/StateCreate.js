import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, MASTER_STATE, buildURL_v1, COUNTRY } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../../common/Common';
import Select from 'react-select';


class StateCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            state: {
                name: '',
                country_code: '',
            },
            error: {
                name: [],
                country_code: [],
            },
            isLoading: false,
            country: [],
            selectedValue: null
        }
    }

    componentDidMount() {
        Axios.get(buildURL(COUNTRY, { "per-page": 100 }))
            .then((res) => {
                this.setCountryList(res.data);
            })
            .catch((error) => {

            });
    }

    setCountryList(data) {
        let mCountry = [];
        data.forEach((item, index) => {
            mCountry.push({ value: item.country_code, label: item.name });
        });
        this.setState({ country: mCountry });
    }

    onHandleChange(e) {
        let mState = this.state.state;
        mState[e.target.name] = e.target.value;
        this.setState({ State: mState });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        Axios.post(buildURL_v1(MASTER_STATE), this.state.state)
            .then((response) => {
                successAlert("State created successfully");
                this.onSuccess();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mstate = this.state.state;
        Object.keys(mstate).forEach((key) => {
            mstate[key] = "";
        });
        this.setState({ state: mstate,selectedValue:null });
    }

    validate() {
        let mstate = this.state.state;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mstate).forEach((key) => {
            if (mstate[key] === "") {
                mError[key].push(key + " Should not be empty");
                isError = true;
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    onCountryChange(value) {
        let mState = this.state.state;
        mState.country_code = value.value;
        this.setState({ State: mState, selectedValue: value });
    }

    render() {

        const { state, error, selectedValue } = this.state;

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Create state'></CardHeader>

                            <CardContent>

                                <Select
                                    name='country_code'
                                    placeholder='Country'
                                    fullWidth
                                    options={this.state.country}
                                    helperText={error.state_code}
                                    value={selectedValue}
                                    error={error.country_code.length > 0 ? true : false}
                                    required
                                    onChange={(value) => this.onCountryChange(value)}
                                />

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={state.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '24px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Create</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default StateCreate;