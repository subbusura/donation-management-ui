import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, MASTER_COUNTRY, buildURL_v1 } from './../../constants/api';
import { validateResponse } from './../../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../../common/Common';
import { Z_STREAM_ERROR } from 'zlib';


class CountryCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            country: {
                name: '',
                country_code: '',
                phone_code: '',
            },
            error: {
                name: [],
                country_code: [],
                phone_code: [],
            },
            isLoading: false
        }
    }

    onHandleChange(e) {
        let mCountry = this.state.country;
        mCountry[e.target.name] = e.target.value;
        this.setState({ country: mCountry });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        Axios.post(buildURL_v1(MASTER_COUNTRY), this.state.country)
            .then((response) => {
                successAlert("Country created successfully");
                this.onSuccess();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mCountry = this.state.country;
        Object.keys(mCountry).forEach((key) => {
            mCountry[key] = "";
        });
        this.setState({ country: mCountry });
    }

    validate() {
        let mCountry = this.state.country;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mCountry).forEach((key) => {
            if (mCountry[key] === "") {
                mError[key].push(key + " Should not be empty");
                isError = true;
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    render() {

        const { country, error } = this.state;

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Create Country'></CardHeader>

                            <CardContent>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={country.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                />
                                <TextField
                                    name='country_code'
                                    label='Country Code'
                                    fullWidth
                                    helperText={error.country_code}
                                    value={country.country_code}
                                    error={error.country_code.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />
                                <TextField
                                    required
                                    name='phone_code'
                                    label='Phone Code'
                                    fullWidth
                                    error={error.phone_code.length > 0 ? true : false}
                                    helperText={error.phone_code}
                                    value={country.phone_code}
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Create</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default CountryCreate;