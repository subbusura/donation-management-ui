
import DashboardPage from './../pages/DashboardPage'
import LoginPage from './../pages/LoginPage';



const Routers=[
    {
        path:'/',
        component:DashboardPage
    },
    {
        path:'/auth/login',
        component:LoginPage
    },
];

export default Routers;