import React from 'react';
import {
    Create,
    FormTab,
    NumberInput,
    ReferenceInput,
    SelectInput,
    TabbedForm,
    DateInput,
    TextInput,
    required,
    SimpleForm
} from 'react-admin';
import withStyles from '@material-ui/core/styles/withStyles';
import { Typography, Card } from '@material-ui/core';
//import RichTextInput from 'ra-input-rich-text';

export const styles = {
    stock: { width: '5em' },
    price: { width: '5em' },
    width: { width: '5em' },
    widthFormGroup: { display: 'inline-block' },
    height: { width: '5em' },
    heightFormGroup: { display: 'inline-block', marginLeft: 32 },
};


class BankCreate extends React.Component {

    render() {

        return (

            <div className='container'>
                <Card>

                    <SimpleForm  >

                        <Typography variant="title">Create Bank</Typography>

                        <TextInput
                            source="Name"
                            fullWidth={true}
                        />
                    </SimpleForm>


                </Card>
            </div>
        )

    }

}



export default withStyles(styles)(BankCreate);
