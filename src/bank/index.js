import BankList from './BankList';
import BankCreate from './BankCreate';
import BankEdit from './BankEdit';

export default {
    list: BankList,
    create: BankCreate,
    edit: BankEdit,
};