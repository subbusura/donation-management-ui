import Alert from 'react-s-alert';

export function commonLog(arg1, arg2) {
    console.log(arg1, arg2);
}

export function successAlert(message) {
    return (
        Alert.success(message, {
            position: 'top-right',
            effect: 'slide',
            timeout: 5000,

        })
    )
}

export function errorAlert(message) {
    return (
        Alert.error(message, {
            position: 'top-right',
            effect: 'slide',
            timeout: 5000,
            //beep: 'http://s-alert-demo.meteorapp.com/beep.mp3'
        })
    )
}
