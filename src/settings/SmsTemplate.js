import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import PersonIcon from '@material-ui/core/Avatar';
import { Route } from "react-router";
import { connect } from "react-redux";
import { Drawer,Button,withStyles} from "@material-ui/core";
import { push as pushAction } from 'react-router-redux';
import { List, TextField, DateField, ReferenceField, EditButton } from "react-admin";
import compose from 'recompose/compose';


import SmsTemplateUpdate from "./SmsTemplateUpdate";


const styles = {
        drawerContent: {
            width: 450
        }
};

const FullNameField = ({ record = {} }) =>{

   return (<span>{record.sms_type.toString().replace("_"," ").replace("_"," ").toUpperCase()}</span>)

};


let handleUpdateClick=(data,push)=>{
    
    push("/setting/sms/template/"+data.sms_type);

}

const cardStyle = {
    width: 400,
    minHeight: 300,
    margin: '0.5em',
    display: 'inline-block',
    verticalAlign: 'top'
};
const CommentGrid = ({ ids, data, basePath,pushAction}) => (
    <div style={{ margin: '1em' }}>
    {ids.map(id =>
        <Card key={id} style={cardStyle}>
            <CardHeader
                title={<FullNameField record={data[id]} />}
                subheader={<DateField record={data[id]} source="created_at" />}
            />
            <CardContent>
                <TextField record={data[id]} source="content" />
            </CardContent>
            <CardActions style={{ textAlign: 'right' }}>
                <Button label="Update" onClick={()=>{handleUpdateClick(data[id],pushAction)}}>Update</Button>;
            </CardActions>
        </Card>
    )}
    </div>
);
CommentGrid.defaultProps = {
    data: {},
    ids: [],
};



  
class SmsTemplate extends React.Component{

    
 
    handleClose = (props) => {
        props.pushAction("/setting/sms/template");
      };

    render(){

        return (

            <React.Fragment>
            <List title="SMS Template" {...this.props}>
                <CommentGrid pushAction={this.props.pushAction}/>
            </List>
        
        <Route
        path="/setting/sms/template/:id" >
        {({ match }) => {
        const isMatch = match && match.params && match.params.id !== "create";
        
         return (
                <Drawer  open={isMatch} anchor="right" onClose={()=>this.handleClose(this.props)}>
                  {/* To avoid any errors if the route does not match, we don't render at all the component in this case */}
                  {isMatch ? (
                    <SmsTemplateUpdate
                      className={this.props.classes.drawerContent}
                      id={match.params.id}
                      onCancel={()=>this.handleClose(this.props)}
                      {...this.props}
                    />
                  ) : null}
                </Drawer>
              );
            }}
        
        </Route>
          </React.Fragment>

        );
    }

}

export default compose(connect(
    undefined,
    { pushAction }
  ),withStyles(styles))(SmsTemplate);

