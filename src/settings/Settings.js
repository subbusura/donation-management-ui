import React from 'react';
import { Button, Grid, Paper, FormControl, FormControlLabel, InputLabel, Input, List, ListItem, ListItemText } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import { buildURL } from '../constants/api';
import { successAlert } from '../common/Common';

class Settings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sms: {
                name: '',
                url: ''
            },
            error: {
                name: '',
                url: ''
            },
            smsTemplates: []
        }
    }

    componentDidMount() {
        this.getSms();
    }

    getSms() {

        Axios.all([
            Axios.get(buildURL("/setting/sms")),
            Axios.get(buildURL("/setting/sms/template")),
        ])
            .then(Axios.spread((sms, smsTemplate) => {
                this.setState({
                    sms: sms.data,
                    smsTemplate: smsTemplate.data
                })
            }))
            .catch((error) => {
                //alert(error);
            })

    }

    onHandleChange(e) {
        let { sms } = this.state;
        sms[e.target.name] = e.target.value;
        this.setState({ sms: sms });
    }

    onSmsUpdate(e) {
        e.preventDefault();

        if (!this.validateSms()) {
            return false;
        }

        Axios.put(buildURL("/setting/sms"), this.state.sms)
            .then((response) => {
                successAlert("Updated successfully");
            })
            .catch((error) => {

            });

    }

    validateSms() {
        let { sms, error } = this.state;
        let mError = true;
        Object.keys(error).forEach((key) => {
            if (sms[key] === "") {
                error[key] = key + " should not be empty";
                mError = false;
            }
        });
        this.setState({ error: error });
        return mError;

    }

    render() {
        return (
            <Grid>

                {this.renderSms()}
                {/* {this.renderTemplates()} */}

            </Grid>
        )
    }

    renderSms() {
        let { sms } = this.state;
        return (
            <Paper square elevation={1} style={{ padding: '16px' }} >

                <h4><strong>SMS</strong></h4>

                <Grid container alignItems="flex-end" spacing={16}>

                    <Grid item xs={1}>Name : </Grid>

                    <Grid item xs={3}>

                        <Input
                            fullWidth
                            onChange={(e) => this.onHandleChange(e)}
                            name="name"
                            placeholder="Name"
                            value={sms.name}
                            style={{ marginTop: '16px' }}
                        />

                    </Grid>

                    <Grid item xs={1}>URL : </Grid>

                    <Grid item xs={5}>

                        <Input
                            name='url'
                            placeholder="URL"
                            fullWidth
                            onChange={(e) => this.onHandleChange(e)}
                            name="url"
                            value={sms.url}
                            style={{ marginTop: '16px' }}

                        />

                    </Grid>

                    <Grid item xs={2}>

                        <Button size="small" color="secondary" variant="contained" onClick={(e) => this.onSmsUpdate(e)} >UPDATE</Button>

                    </Grid>

                </Grid>

            </Paper>
        )
    }

    renderTemplates() {
        let { smsTemplates } = this.state;
        return (
            <Paper square elevation={1} style={{ marginTop: '16px', padding: '16px' }}>

                <h4><strong>SMS Template</strong></h4>

                <Grid container spacing={16}>

                    <Grid item xs={3}>

                        <List component="nav" aria-label="main mailbox folders">
                            <ListItem
                                button
                                selected={true}
                                style={{ background: '#f0f8ff' }}
                            // onClick={event => handleListItemClick(event, 0)}
                            >
                                <ListItemText primary="Payment Received" />
                            </ListItem>
                            <ListItem
                                button
                                selected={false}
                            // onClick={event => handleListItemClick(event, 0)}
                            >
                                <ListItemText primary="Remainder" />
                            </ListItem>
                        </List>

                    </Grid>

                    <Grid item xs={6}>

                        <TextField
                            required
                            margin="normal"
                            variant="outlined"
                            fullWidth
                            multiline
                            value={""}
                            style={{ border: "1px" }}
                        />
                        <Grid container justify="flex-end">
                            <Button size="small" color="secondary" variant="contained" >UPDATE</Button>
                        </Grid>

                    </Grid>

                </Grid>

            </Paper>

        )
    }

}

export default Settings;
