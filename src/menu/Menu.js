import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import LabelIcon from '@material-ui/icons/Label';
import { withRouter } from 'react-router-dom';
import {
    translate,
    DashboardMenuItem,
    MenuItemLink,
    Responsive,
} from 'react-admin';

// import visitors from '../visitors';
// import orders from '../orders';
// import invoices from '../invoices';
// import products from '../products';
// import categories from '../categories';
// import reviews from '../reviews';
import SubMenu from './SubMenu';
import { SUPER_USER, ADMIN } from '../constants/Constants';
import { Icon } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import PendingIcon from '@material-ui/icons/HourglassFull';
import ReceiveIcon from '@material-ui/icons/SystemUpdateAlt';
import SettingsIcon from '@material-ui/icons/Settings';
import SubscriptionIcon from '@material-ui/icons/TouchApp';
import AddIcon from '@material-ui/icons/Add';
import PeopleIcon from '@material-ui/icons/People';
import MasterIcon from '@material-ui/icons/Build';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const imageStyle = { with: '16px', height: '16px' };

class Menu extends Component {
    state = {
        menuCatalog: false,
        menuDM: false,
        menuMMM: false,
        menuPayment: false,
        menuCustomers: false,
    };

    static propTypes = {
        onMenuClick: PropTypes.func,
        logout: PropTypes.object,
    };

    handleToggle = menu => {
        this.setState(state => ({ [menu]: !state[menu] }));
    };

    render() {
        const { onMenuClick, open, logout, translate } = this.props;
        let user = JSON.parse(localStorage.getItem("user_account"));
        return (
            <div>
                {' '}
                <DashboardMenuItem onClick={onMenuClick} />
                <MenuItemLink
                    to={'/member'}
                    primaryText={'Member'}
                    leftIcon={<PeopleIcon />}
                    onClick={onMenuClick}
                />
                {/* <MenuItemLink
                        to={`/broadcast`}
                        primaryText={translate(`Broadcast`, {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    />
                    <MenuItemLink
                        to={`/receipt`}
                        primaryText={translate(`Receipt`, {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    />
                    <MenuItemLink
                        to={`/reports`}
                        primaryText={translate(`Reports`, {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    /> */}

                {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1" ?

                    <SubMenu
                        handleToggle={() => this.handleToggle('menuDM')}
                        isOpen={this.state.menuDM}
                        sidebarIsOpen={open}
                        name="Masters"
                        icon={<MasterIcon />}
                    >
                        <MenuItemLink
                            to={'/master/country'}
                            primaryText={'Country'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/country.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                        <MenuItemLink
                            to={`/master/state`}
                            primaryText={'State'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/state.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                        <MenuItemLink
                            to={`/master/city`}
                            primaryText={'City'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/city.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                        <MenuItemLink
                            to={`/master/area`}
                            primaryText={'Area'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/area.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                        <MenuItemLink
                            to={`/master/bank`}
                            primaryText={'Bank'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/banks.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                        <MenuItemLink
                            to={`/master/organization`}
                            primaryText={'Organization'}
                            leftIcon={<Icon><img style={imageStyle} src='./images/organization.png' /></Icon>}
                            onClick={onMenuClick}
                        />
                    </SubMenu>
                    : ""
                }

                {/* <SubMenu
                    handleToggle={() => this.handleToggle('menuMMM')}
                    isOpen={this.state.menuMMM}
                    sidebarIsOpen={open}
                    name="MMM"
                    icon={<LabelIcon />}
                >
                    <MenuItemLink
                        to={'/mmm/profile'}
                        primaryText={translate('Profile', {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    />
                    <MenuItemLink
                        to={`/mmm/receipt`}
                        primaryText={translate(`Receipt`, {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    />
                    <MenuItemLink
                        to={`/mmm/reports`}
                        primaryText={translate(`Reports`, {
                            smart_count: 2,
                        })}
                        leftIcon={<LabelIcon />}
                        onClick={onMenuClick}
                    />
                </SubMenu> */}

                <SubMenu
                    handleToggle={() => this.handleToggle('menuPayment')}
                    isOpen={this.state.menuPayment}
                    sidebarIsOpen={open}
                    name="Payment"
                    icon={<Icon><img style={{ width: '20px', height: '20px' }} src='./images/rupee.png' /></Icon>}
                >
                    <MenuItemLink
                        to={'/payment'}
                        primaryText={'Received'}
                        leftIcon={<ReceiveIcon />}
                        onClick={onMenuClick}
                    />
                    <MenuItemLink
                        to={'/payment/pending'}
                        primaryText={'Pending'}
                        leftIcon={<PendingIcon />}
                        onClick={onMenuClick}
                    />
                    {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1" ?
                        <MenuItemLink
                            to={'/payment/add'}
                            primaryText={'Add'}
                            leftIcon={<AddIcon />}
                            onClick={onMenuClick}
                        /> : ""
                    }
                </SubMenu>

                <MenuItemLink
                    to={'/recurring'}
                    primaryText={'Recurring'}
                    leftIcon={<SubscriptionIcon />}
                    onClick={onMenuClick}
                />

                {localStorage.getItem(SUPER_USER) === "1" ?
                    <MenuItemLink
                        to={`/settings`}
                        primaryText={'Settings'}
                        leftIcon={<SettingsIcon />}
                        onClick={onMenuClick}
                    />
                    : ""
                }

                {localStorage.getItem(SUPER_USER) === "1" ?
                    <MenuItemLink
                        to={`/setting/sms/template`}
                        primaryText={'SMS Template'}
                        leftIcon={<SettingsIcon />}
                        onClick={onMenuClick}
                    />
                    : ""
                }   

                {localStorage.getItem(SUPER_USER) === "1" ?

                    <MenuItemLink
                        to={`/user`}
                        primaryText={'User'}
                        leftIcon={<PersonIcon />}
                        onClick={onMenuClick}
                    />
                    : ""
                }
                {localStorage.getItem(SUPER_USER) === "1" ?

                <MenuItemLink
                    to={`/member/import`}
                    primaryText={'import'}
                    leftIcon={<ArrowDownwardIcon />}
                    onClick={onMenuClick}
                />
: ""
}

                {/* <MenuItemLink
                    to={`/bank`}
                    primaryText={translate(`Bank`, {
                        smart_count: 2,
                    })}
                    leftIcon={<LabelIcon />}
                    onClick={onMenuClick}
                />

                <MenuItemLink
                    to={`/transaction-type`}
                    primaryText={translate(`Transaction Type`, {
                        smart_count: 2,
                    })}
                    leftIcon={<LabelIcon />}
                    onClick={onMenuClick}
                /> */}


                <Responsive
                    small={logout}
                    medium={null} // Pass null to render nothing on larger devices
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    open: state.admin.ui.sidebarOpen,
    theme: state.theme,
    locale: state.i18n.locale,
});

const enhance = compose(
    withRouter,
    connect(
        mapStateToProps,
        {}
    ),
    translate
);

export default enhance(Menu);