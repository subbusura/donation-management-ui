import React, { Component } from 'react'
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, MenuItem, Select, IconButton, Chip, Input, } from '@material-ui/core';
import Axios from 'axios';
import { buildURL, USER_CREATE, buildURL_v1 } from '../constants/api';
import { validateResponse } from '../constants/ResponseValidation';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { successAlert, errorAlert } from '../common/Common';
import DeleteIcon from '@material-ui/icons/Delete';
import { confirmAlert } from 'react-confirm-alert';
import { roles } from '../constants/Constants';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default class UserUpdate extends Component {

    constructor(props) {
        super(props);
        this.userId = this.props.match.params.id;
        this.state = {
            user: {
                first_name: '',
                username: '',
                email: '',
                mobile_number: '',
                role: '',
                last_name: '',
                status: 10,
                permissions: [],
            },
            error: {
                first_name: '',
                username: '',
                email: '',
                mobile_number: '',
                role: '',
                last_name: '',
                status: '',
                permissions: [],
            },
            isLoading: false,
            districtList: [],
        }
    }

    componentDidMount() {

        Axios.all([
            Axios.get(buildURL(`/user/${this.userId}`)),
            Axios.get(buildURL_v1("/master/city", { "per-page": 1000 })),
        ])
            .then(Axios.spread((response, districtList) => {
                this.setState({ user: response.data })
                this.setState({ districtList: districtList.data.items });
            }))
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });

    }

    onHandleChange(e) {
        let mUser = this.state.user;
        mUser[e.target.name] = e.target.value;
        this.setState({ user: mUser });
    };

    onpermissionsChange(e) {
        let mUser = this.state.user;
        mUser.permissions = e.target.value;
        this.setState({ user: mUser });
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }
        this.setState({ isLoading: true });
        Axios.put(buildURL(`/user/${this.userId}`), this.state.user)
            .then((response) => {
                successAlert("Updated successfully");
                window.history.back();
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    this.setError(error.response.data);
                } else {
                    window.alert(response.message);
                }
            });

    }

    onSuccess() {
        let mUser = this.state.user;
        Object.keys(mUser).forEach((key) => {
            mUser[key] = "";
        });
        this.setState({ user: mUser });
    }

    validate() {
        let mUser = this.state.user;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mUser).forEach((key) => {
            if (key !== "last_name") {
                if (mUser[key] === "") {
                    mError[key] = key + " Should not be empty";
                    isError = true;
                } else {
                    mError[key] = "";
                }
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    setError(mError) {
        let { error } = this.state;
        Object.keys(mError).forEach((key) => {
            error[key] = mError[key];
        });
        this.setState({ error: error });
    }

    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this user?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteUser() }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });

    }

    deleteUser() {
        Axios.delete(buildURL(`/user/${this.userId}`))
            .then((res) => {
                successAlert("User deleted successfully");
                window.history.back();

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }


    render() {

        const { user, error, isLoading , districtList} = this.state;

        let role = roles.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });

        return (
            <form onSubmit={(e) => this.onSubmit(e)}>
                <Grid container justify="center" spacing="8" >
                    <Grid item xs={5}>

                        <Card>


                            <CardContent>

                                <Grid
                                    container
                                    direction="row"
                                    justify="space-between"
                                    alignItems="baseline">

                                    <h2>Update User</h2>
                                    <IconButton> <DeleteIcon onClick={(e) => this.onDelete(e)} /> </IconButton>
                                </Grid>

                                <TextField
                                    name="first_name"
                                    id="filled-firstname"
                                    label="FirstName"
                                    // className={classes.textField}
                                    value={user.first_name}
                                    onChange={(e) => this.onHandleChange(e)}
                                    margin="normal"
                                    variant="filled"
                                    fullWidth
                                />
                                <TextField
                                    name="last_name"
                                    id="filled-lastname"
                                    label="Lastname"
                                    value={user.last_name}
                                    fullWidth
                                    // className={classes.textField}
                                    margin="normal"
                                    variant="filled"
                                    onChange={(e) => this.onHandleChange(e)}
                                />
                                <TextField
                                    name='email'
                                    label='Email'
                                    fullWidth
                                    helperText={error.email}
                                    value={user.email}
                                    error={error.email.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />
                                <TextField
                                    name='mobile_number'
                                    label='Mobile Number'
                                    fullWidth
                                    helperText={error.mobile_number}
                                    value={user.mobile_number}
                                    error={error.mobile_number.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px', marginBottom: '24px' }}
                                />

                            </CardContent>
                        </Card>
                    </Grid>

                    <Grid item xs={5}>
                        <Card>

                            <CardContent>

                                <Grid
                                    container
                                    direction="row"
                                    justify="space-between"
                                    alignItems="baseline">
                                    <h2>Login Detail</h2>
                                </Grid>

                                <TextField
                                    required
                                    name='username'
                                    label='Username'
                                    fullWidth
                                    error={error.username.length > 0 ? true : false}
                                    helperText={error.username}
                                    value={user.username}
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />

                                <Grid container spacing={8} >
                                    <Grid item xs={6}>
                                        <FormControl fullWidth style={{ marginTop: '16px' }} >
                                            <InputLabel >Role</InputLabel>
                                            <Select
                                                label='role'
                                                value={user.role}
                                                onChange={(e) => this.onHandleChange(e)}
                                                fullWidth
                                                name='role'
                                                style={{ marginTop: '16px' }}
                                            >
                                                {role}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6}>

                                        <FormControl fullWidth style={{ marginTop: '16px' }} >
                                            <InputLabel >Status</InputLabel>
                                            <Select
                                                label='Status'
                                                value={user.status}
                                                onChange={(e) => this.onHandleChange(e)}
                                                fullWidth
                                                name='status'
                                                style={{ marginTop: '16px' }}
                                            >
                                                <MenuItem value={10}>Active</MenuItem>
                                                <MenuItem value={0}>Inactive</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                </Grid>

                                {
                                    user.role === "district admin" ?
                                        <FormControl fullWidth style={{ marginTop: '16px' }} >
                                            <InputLabel id="demo-mutiple-chip-label">District</InputLabel>
                                            <Select
                                                labelId="demo-mutiple-chip-label"
                                                id="demo-mutiple-chip"
                                                multiple
                                                value={user.permissions}
                                                onChange={(e) => this.onpermissionsChange(e)}
                                                input={<Input id="select-multiple-chip" />}
                                                renderValue={selected => (
                                                    <div style={{ display: 'flex', flexWrap: 'wrap' }} >
                                                        {selected.map(value => (
                                                            <Chip style={{ margin: '2px' }} key={value} label={value} />
                                                        ))}
                                                    </div>
                                                )}
                                                MenuProps={MenuProps}
                                            >
                                                {districtList.map((item, index) => {
                                                    return (
                                                        <MenuItem key={index} value={item.name} >
                                                            {item.name}
                                                        </MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        </FormControl>
                                        : ""
                                }

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                        disabled={isLoading}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                        disabled={isLoading}
                                    >Update</Button>
                                </Grid>

                            </CardActions>

                        </Card>

                    </Grid>

                </Grid>

            </form >
        )

    }
}
