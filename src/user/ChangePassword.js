import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { change, submit, isSubmitting,stopSubmit,reset} from "redux-form";
import {
  fetchEnd,
  fetchStart,
  required,
  showNotification,
  Button,
  SaveButton,
  SimpleForm,
  TextInput,
  LongTextInput,
  CREATE,
  REDUX_FORM_NAME,
} from "react-admin";
import IconContentAdd from "@material-ui/icons/Add";
import IconCancel from "@material-ui/icons/Cancel";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";

import dataProvider from "./../dataprovider";

class ChangePassword extends Component {
  state = {
    error: false,
    isSubmitting:false,
  };

  handleClick = () => {
    this.setState({ isSubmitting: true });
  };

  handleCloseClick = () => {
    this.setState({ showDialog: false });
  };

  handleSaveClick = () => {
    const { submit } = this.props;
    submit("password-change-update");
  };

  handleSubmit = values => {
    const { change, fetchStart, fetchEnd, showNotification,stopSubmit ,reset ,onCancel} = this.props;


    if(this.state.isSubmitting){
       return false;
    }

    fetchStart();
    this.setState({ isSubmitting: true });

    // As we want to know when the new post has been created in order to close the modal, we use the
    // dataProvider directly
    dataProvider("PASSWORD_CHANGE", "user", { id:this.props.id,data: values })
      .then(({ data }) => {
        // Update the main react-admin form (in this case, the comments creation form)
       

          if(Object.keys(data).length!=0){

            stopSubmit("password-change-update",data)


          }else{

             reset("password-change-update")
             showNotification("Password has been changed successfully", "success");
             onCancel();
          }

         

      })
      .catch(error => {
        showNotification(error.message, "error");
      })
      .finally(() => {
        // Dispatch an action letting react-admin know a API call has ended
        fetchEnd();
        this.setState({ isSubmitting: false });
      });

   
  };

  render() {
   
    const {isSubmitting} = this.state;
    const { onCancel } = this.props;

    return (
      <Fragment>
        <SimpleForm
              // We override the redux-form name to avoid collision with the react-admin main form
              form="password-change-update"
              resource="users"
              // We override the redux-form onSubmit prop to handle the submission ourselves
              onSubmit={this.handleSubmit}
              // We want no toolbar at all as we have our modal actions
              toolbar={null}
            >
              <TextInput source="new_password" validate={required()} />
             <TextInput source="confirm_password" validate={required()} />

            </SimpleForm>

            <SaveButton saving={isSubmitting} onClick={this.handleSaveClick} />
            <Button label="ra.action.cancel" onClick={onCancel}>
              <IconCancel />
            </Button>

      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: isSubmitting("password-change-update")(state),
});

const mapDispatchToProps = {
  change,
  fetchEnd,
  fetchStart,
  showNotification,
  submit,
  stopSubmit,
  reset
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangePassword);