import React, { Component } from 'react'
import { UPDATE} from 'react-admin';
import dataProvider from './../dataprovider';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { push as pushAction } from 'react-router-redux';
import { showNotification as showNotificationAction } from 'react-admin';
class ChangePasswordButton extends Component {
    constructor(props){
        super(props)
        
    }
    handleClick = () => {
        const { push, record, showNotification } = this.props;

        push("/user/"+record.id+"/password")
    }

    render() {
        return <Button label="Password Change" onClick={this.handleClick}>Password Change</Button>;
    }
}

export default connect(null, {
    push: pushAction,
})(ChangePasswordButton);