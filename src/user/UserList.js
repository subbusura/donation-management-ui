import React from 'react';
import { List, Datagrid, TextField, EmailField,CardActions,
    CreateButton } from 'react-admin';
import ChangePasswordButton from "./ChangePasswordButton";
import { Route } from "react-router";
import { Drawer } from "@material-ui/core";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import ChangePassword from "./ChangePassword"


class UserList extends React.Component{

    handleClose = () => {
        this.props.push("/user");
      };
        render(){
            const props = this.props;
            return (
                <React.Fragment>
                    <List {...props}
                  bulkActionButtons={false}
                >
        <Datagrid >
            <TextField source="first_name" />
            <TextField source="last_name" />
            <TextField source="username" />
            <EmailField source="email" />
            <TextField source="mobile_number" />
            <TextField source="role" />            
            <TextField source="status" />
            <ChangePasswordButton />
        </Datagrid>
    </List>
            <Route
                path="/user/:id/password" >
                {({ match }) => {
         const isMatch = match && match.params && match.params.id !== "create";

                 return (
                        <Drawer open={isMatch} anchor="right" onClose={this.handleClose}>
                          {/* To avoid any errors if the route does not match, we don't render at all the component in this case */}
                          {isMatch ? (
                            <ChangePassword
                              id={match.params.id}
                              onCancel={this.handleClose}
                              {...props}
                            />
                          ) : null}
                        </Drawer>
                      );
                    }}

              </Route>
               
                </React.Fragment>
            );
            

        }

}


export default connect(
    undefined,
    { push }
  )(UserList);
  
