import React from 'react';
import { List, Datagrid, TextField, Pagination, TextInput, Filter, DateField, SelectInput, downloadCSV } from 'react-admin';
import { Button } from '@material-ui/core';
import CustomInput from './../payment/CustomInput';
import DateFilter from './../payment/DateFilter';
import { paymentType, recurringType, donationType, SUPER_USER, getDistricts, USER_ACCOUNT } from '../constants/Constants';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="name" alwaysOn />
        <SelectInput
            source="type"
            choices={recurringType}
            allowEmpty={false}
        />

        <DateFilter
            source="start_date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="start_date_range.from"
            toSource="start_date_range.to"
            label="Start Time"
        />
        <DateFilter
            source="next_date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="next_date_range.from"
            toSource="next_date_range.to"
            label="Next Due"
        />
    </Filter>
);

const exporter = list => {
    const exportableMember = list.map((item) => {
        item.member = item.member.name;
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['member', 'type', 'start', 'next_due']
    });
    downloadCSV(csv, 'Recurrings');
}

const SubscriptionList = props => (

    <List {...props}
        bulkActionButtons={false}
        filters={<PostFilter />}
        exporter={exporter}
        pagination={<Pagination perPage={500} rowsPerPageOptions={[50,100,500]}/>}
    >
        <Datagrid rowClick="edit">
            {/* <TextField source="id" /> */}
            <TextField label="Member" source="member.name" />
            <TextField label="Recurring Type" source="type" />
            <TextField source="start" />
            <TextField source="next_due" />
            <TextField label="Status" source="state" />
        </Datagrid>
    </List>
);

export default SubscriptionList;
