import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'react-admin';
import Axios from 'axios';
import { buildURL, LOGIN, LOGOUT_USER } from './constants/api';
import { validateResponse } from './constants/ResponseValidation';
import { commonLog, errorAlert } from './common/Common';
import { USER_ACCOUNT, SUPER_USER, ADMIN } from './constants/Constants';

export default (type, params) => {
    // called when the user attempts to log in
    if (type === AUTH_LOGIN) {

        return doLogin(params);
    }
    // called when the user clicks on the logout button
    if (type === AUTH_LOGOUT) {
        //logoutUser(false);
        localStorage.removeItem('user');
        localStorage.clear();
        return Promise.resolve();
    }
    // called when the API returns an error
    if (type === AUTH_ERROR) {
        const { status, message } = params;
        if (status === 401 || status === 403 || message == 'Unauthorized access') {
            //logoutUser(true);
            localStorage.removeItem('user');
            localStorage.clear();
            return Promise.reject();
        } else {
            return Promise.resolve();
        }

    }
    // called when the user navigates to a new location
    if (type === AUTH_CHECK) {
        return localStorage.getItem('user')
            ? Promise.resolve()
            : Promise.reject();
    }
    // return Promise.reject('Unknown method');
};


function doLogin(params) {

    return Axios.post(buildURL(LOGIN), params)
        .then((response) => {
            //commonLog("Response", response);
            let user = {
                username: params.username,
                token: response.data.access_token
            };
            localStorage.setItem('user', user.token);
            localStorage.setItem(USER_ACCOUNT, JSON.stringify(response.data.user));
            localStorage.setItem(SUPER_USER, response.data.user.role.toUpperCase() === "SUPER USER" ? "1" : "0" );
            localStorage.setItem(ADMIN, response.data.user.role.toUpperCase() === "ADMIN" ? "1" : "0" );
            Axios.defaults.headers.common['Authorization'] = `${user.token}`;
        })
        .catch((error) => {
            let response = validateResponse(error.response);
            if (response.isValidation) {
                window.alert(error.response.data.password[0]);

            } else {
                window.alert(response.message);

            }
        });

}

function logoutUser(isAuthFailed) {

    return Axios.delete(buildURL(LOGOUT_USER))
        .then((response) => {
            localStorage.removeItem('user');
            localStorage.clear();
        })
        .catch((error) => {
            let response = validateResponse(error.response.data);
            if (response.isValidation) {
                window.alert(error.response.data.errors[0]);
            } else {
                window.alert(response.message);
            }
        });

}