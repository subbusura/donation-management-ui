import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, APPLICATION_CREATE } from '../constants/api';
import { validateResponse } from '../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog } from '../common/Common';


class ApplicationCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            application: {
                name: '',
                description: '',
                server: '',
                type: ''
            },
            error: {
                name: '',
                description: '',
                server: '',
                type: ''
            },
            isLoading: false
        }
    }

    onHandleChange(e) {
        let mApplication = this.state.application;
        mApplication[e.target.name] = e.target.value;
        this.setState({ application: mApplication });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        // Axios.post(buildURL(APPLICATION_CREATE), this.state.application)
        //     .then((response) => {
        //         window.alert("Application created successfully");
        //         this.onSuccess();
        //     })
        //     .catch((error) => {
        //         let response = validateResponse(error.response.data);
        //         if (response.isValidation) {
        //             window.alert(error.response.data.errors[0].message);
        //         } else {
        //             window.alert(response.message);
        //         }
        //     });

    }

    onSuccess() {
        let mApplication = this.state.application;
        Object.keys(mApplication).forEach((key) => {
            mApplication[key] = "";
        });
        this.setState({ application: mApplication });
    }

    validate() {
        let mApplication = this.state.application;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mApplication).forEach((key) => {
            if (mApplication[key] === "") {
                mError[key] = key + " Should not be empty";
                isError = true;
            } else {
                mError[key] = "";
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    render() {

        // const createForm = props => (
        //     <Create {...props} >

        //         <SimpleForm>

        //             <TextInput
        //                 source="name"
        //                 label="Name"
        //                 optioons={{ fullWidth: true }}
        //                 fullWidth
        //             />
        //             <LongTextInput
        //                 source="description"
        //                 label="Description"
        //                 optioons={{ fullWidth: true }}
        //                 fullWidth
        //             />
        //             <TextInput
        //                 source="server"
        //                 label="Server"
        //                 optioons={{ fullWidth: true }}
        //                 fullWidth
        //             />
        //         </SimpleForm>

        //     </Create >
        // );

        const { application, error } = this.state;

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={6}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <Card>

                            <CardHeader title='Create Application'></CardHeader>

                            <CardContent>

                                <TextField
                                    name='name'
                                    label='Name'
                                    fullWidth
                                    helperText={error.name}
                                    value={application.name}
                                    error={error.name.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                />
                                <TextField
                                    required
                                    name='description'
                                    label='Description'
                                    fullWidth
                                    multiline
                                    rowsMax="6"
                                    error={error.description.length > 0 ? true : false}
                                    helperText={error.description}
                                    value={application.description}
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />
                                <TextField
                                    name='server'
                                    label='Server'
                                    fullWidth
                                    helperText={error.server}
                                    value={application.server}
                                    error={error.server.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />
                                <TextField
                                    name='type'
                                    label='Type'
                                    fullWidth
                                    helperText={error.type}
                                    value={application.type}
                                    error={error.type.length > 0 ? true : false}
                                    required
                                    onChange={(e) => this.onHandleChange(e)}
                                    style={{ marginTop: '16px' }}
                                />

                            </CardContent>

                            <CardActions>
                                <Grid container justify="flex-end" alignItems="right" >
                                    <Button
                                        variant='raised'
                                        style={{ margin: '8px' }}
                                        onClick={(e) => { window.history.back() }}
                                    >Cancel</Button>
                                    <Button
                                        variant='raised'
                                        color="primary"
                                        style={{ margin: '8px' }}
                                        onClick={(e) => this.onSubmit(e)}
                                    >Create</Button>
                                </Grid>

                            </CardActions>

                        </Card>
                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default ApplicationCreate;