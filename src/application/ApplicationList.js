import React from 'react';
// import { List, Datagrid, TextField, EmailField } from 'react-admin';
// import { List, Datagrid, TextField, EmailField, UrlField } from 'react-admin';
import { List, Datagrid, TextField, EditButton, DeleteButton } from 'react-admin';
import Axios from 'axios';
import { buildURL, APPLICATION_LIST } from '../constants/api';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';

// export const DELETE = props =>(
//     <Icon/>
// )

export const ApplicationList = props => (

    <List {...props}
        bulkActionButtons={false}
    >
        <Datagrid rowClick="edit">
            <TextField source="name" />
            <TextField source="appId" />
            <TextField source="description" />
            <TextField source="createdAt" />
            {/* <EditButton/> */}
            <DeleteButton/>
        </Datagrid>
    </List>
);

// class ApplicationList extends React.Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             list: [],
//         }
//     }

//     componentDidMount() {
//         Axios.get(buildURL(APPLICATION_LIST))
//             .then((response) => {
//                 console.log(response.data);
//                 this.setState({ list: response.data.data.data });
//             })
//             .catch((error) => {
//                 console.log("error : ", error.response.data);
//                 if (error.response.data.status == 401) {
//                     window.alert("Unauthorized access");
//                 }
//             });
//     }

//     render() {


//         let items = this.state.list.map((item, index) => {
//             return (
//                 <TableRow>
//                     <TableCell>{(index + 1)}</TableCell>
//                     <TableCell>{item.name}</TableCell>
//                     <TableCell>{item.appId}</TableCell>
//                     <TableCell>{item.description}</TableCell>
//                     <TableCell>{item.createdAt}</TableCell>
//                 </TableRow>
//             )
//         })

//         return (

//             <Table>
//                 <TableHead>
//                     <TableRow>
//                         <TableCell>S.no</TableCell>
//                         <TableCell align="right">Name</TableCell>
//                         <TableCell align="right">App ID</TableCell>
//                         <TableCell align="right">Description</TableCell>
//                         <TableCell align="right">Created</TableCell>
//                     </TableRow>
//                 </TableHead>
//                 <TableBody> {items} </TableBody>
//             </Table>

//         )

//     }

// }

// export default ApplicationList;