import React from 'react';
import {
    Create,
    FormTab,
    NumberInput,
    ReferenceInput,
    SelectInput,
    TabbedForm,
    DateInput,
    TextInput,
    required,
    SimpleForm
} from 'react-admin';
import withStyles from '@material-ui/core/styles/withStyles';
import { Typography } from '@material-ui/core';
//import RichTextInput from 'ra-input-rich-text';

export const styles = {
    stock: { width: '5em' },
    price: { width: '5em' },
    width: { width: '5em' },
    widthFormGroup: { display: 'inline-block' },
    height: { width: '5em' },
    heightFormGroup: { display: 'inline-block', marginLeft: 32 },
};

const organization = [
    { id: 'TMKI', name: 'TMKI' },
    { id: 'BHT', name: 'BHT' }
];
const receipt_type = [
    { id: 'Manual', name: 'Manual' },
    { id: 'E-Receipt', name: 'E-Receipt' }
]

const ProductCreate = ({ classes, ...props }) => (
    <Create  {...props}>

        <SimpleForm  >

            <Typography variant="title">Donation Entry</Typography>

            <SelectInput source="organization" choices={organization}/>

            <SelectInput source="receipt_type" choices={receipt_type}/>



        </SimpleForm>


    </Create>
);

export default withStyles(styles)(ProductCreate);
