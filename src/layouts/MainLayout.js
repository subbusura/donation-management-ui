import React, { Component } from 'react';


export default class MainLayout extends Component {

    render() {

        return (
            // <div>
            //     <h1>Layout Header</h1>
            //     <h1>Layout Search</h1>
            //     <h1>Layout Side Menu</h1>
            //     {React.cloneElement(this.props.children, { ...this.props })}
            //     <h1>Layout Content</h1>
            //     <h1>Layout Fooder</h1>
            // </div>

            <div className={"wrapper"} style={{ height: 'auto', minHeight: '100%' }} >
                <div className={"main-header"}>
                    <nav className={"navbar navbar-static-top"}>
                        <div className={"container"}>
                            <div className={"navbar-header"}>
                                <a href="/" className="navbar-brand"><b>Donation management</b></a>
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <i className={"fa fa-bars"}></i>
                                </button>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>


        );

    }

}