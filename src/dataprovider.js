import { stringify } from 'query-string';
import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    UPDATE_MANY,
    DELETE,
    DELETE_MANY,
    GET_MANY,
    GET_MANY_REFERENCE,
} from 'react-admin';
import { validateResponse } from './constants/ResponseValidation';
import { file } from '@babel/types';

const apiUrl = '/api';
const apiUrl_v1 = '/api/v1';

/**
 * Maps react-admin queries to my REST API
 *
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for a data response
 */

export default (type, resource, params) => {
    console.log("DATA PROVIDER ",type,resource,params)
    const token = localStorage.getItem('user');
    let url = resource.includes("master/") ? apiUrl : apiUrl_v1;
    const options = {
        headers: new Headers({
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `${token}`
        }),
    };
    switch (type) {
        /* Prepare url and options as above */
        case GET_LIST:
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            let lsort = order=="ASC"?"-"+field:field
            const query = {
                sort: lsort,
                'per-page': perPage,
                page: page,
                range: JSON.stringify([
                    (page - 1) * perPage,
                    page * perPage - 1,
                ]),
                filter: JSON.stringify(params.filter),
            };
            console.log("DATA PROVIDER QUERY",query)
            url = `${url + "/" + resource}?${stringify(query)}`;
            break;
        case DELETE:
            url = `${url}/${resource}/${params.id}`;
            options.method = 'DELETE';
            break;
        case GET_ONE:

        const mquery = {
            sms_type:params.id,
            filter: JSON.stringify({sms_type:params.id}),
        };
        console.log("DATA PROVIDER QUERY",mquery)
        url = `${url + "/" + resource}?${stringify(mquery)}`;
        
         break;
         case UPDATE:

            url = `${url}/${resource}/${params.id}`;
            options.method = 'PUT';
            options.body=JSON.stringify(params.data);
            
             break;
        case "PASSWORD_CHANGE":
            url = `${url}/${resource}/${params.id}/password`;
                console.log("DATA PROVIDER PASSWORD",params)
             options.method = 'POST';
             options.body=JSON.stringify(params.data);
            break;
         case DELETE_MANY:
             const query1 = {
                 filter: JSON.stringify({ id: params.ids }),
             };
             url = `${apiUrl}/${resource}?${stringify(query1)}`;
             options.method = 'DELETE';
             break;
         case GET_MANY:
             const query2 = {
                 filter: JSON.stringify({ id: params.ids }),
             };
             url = `${apiUrl}/${resource}?${stringify(query2)}`;
             options.method = 'GET';
             break;
    }
    let headers;
    return fetch(url, options)
        .then(res => {
            headers = res.headers;
            let mResponse = res.json();
            if (res.status == 200) {
                return mResponse;
            }else if(res.status==422) {
                //validateResponse(res);
               // throw new Error(validateResponse(res).message);
               console.log("Validation Error")
               return mResponse;
            }else if(res.status==201) {
                //validateResponse(res);
               // throw new Error(validateResponse(res).message);
              
               return {};
            }else{

                   throw new Error(validateResponse(res).message);

            }
        })
        .then(json => {
            switch (type) {
                case GET_LIST:

                    return {
                        data: json.items,
                        total: json._meta.totalCount
                    }
                case UPDATE:
                      return {
                          data:json
                      }
                    break;
                case GET_ONE:
                       
                    return {
                        data: {...json.items[0],id:params.id},
                       
                    }

                case GET_MANY_REFERENCE:

                    let results = [];
                    // if (json.data.data.length > 0) {
                    //     json.data.data.forEach(element => {
                    //         results.push({
                    //             id: element._id, ...element
                    //         })
                    //     });
                    // }
                    return {
                        data: json.items,
                        total: json._meta.totalCount
                    }
                case CREATE:
                    return { data: { ...params.data, id: json.id } };
                case GET_MANY:
                    return {
                            data:[],
                            total: 1
                    }
                case DELETE_MANY:
                    return { data: json || [] };
                default:
                    return { data: json };
            }
        })
       .catch((error) => {
         console.log("error", error);
      
     });
};