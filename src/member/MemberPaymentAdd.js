import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, Icon, IconButton, FormControl, Checkbox, FormControlLabel, MenuItem, Select, FormHelperText, InputLabel } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Axios from 'axios';
import { buildURL, MASTER_BANK, MASTER_ORGANIZATION, buildURL_v1, BANK, MEMBER } from '../constants/api';
import { validateResponse } from '../constants/ResponseValidation';
import { commonLog, errorAlert, successAlert } from '../common/Common';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { donationType, paymentType } from '../constants/Constants';


class MemberPaymentAdd extends React.Component {

    constructor(props) {
        super(props);
        this.id = this.props.id;
        this.date = new Date();
        this.state = {
            payment: {
                member_id: this.props.id,
                organization_id: '',
                bank_name: '',
                payment_type: '',
                scheme_type: '',
                amount: '',
                cheque_number: '',
                cheque_date: '',
                sms: 0,
                payment_date: this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).toString().slice(-2) + "-" + ("0" + this.date.getDate()).toString().slice(-2),
            },
            error: {
                member_id: [],
                organization_id: [],
                bank_name: [],
                payment_type: [],
                scheme_type: [],
                amount: [],
                cheque_number: [],
                cheque_date: [],
                payment_date: []
            },
            organizationList: [],
            bankList: [],
            memberList: [],
            isLoading: false
        }
    }

    componentDidMount() {
        this.getInitianData();
    }

    getPayment() {
        Axios.get(buildURL(`/payment/${this.id}`))
            .then((res) => {
                //commonLog("PRops", res.data.items)
                this.setState({ payment: res.data });
            })
            .catch((error) => {

            });
    }

    getInitianData() {

        Axios.all([
            Axios.get(buildURL_v1(MASTER_ORGANIZATION)),
            Axios.get(buildURL(BANK))
        ])
            .then(Axios.spread((organization, bank) => {
                this.setState({ organizationList: organization.data.items });
                this.setState({ bankList: bank.data });
            }))
            .catch((error) => {
                validateResponse(error.response);
            });

    }

    onHandleChange(e) {
        let mpayment = this.state.payment;
        mpayment[e.target.name] = e.target.value;
        if (e.target.name === 'payment_type') {
            mpayment.cheque_number = "";
        }
        this.setState({ payment: mpayment });
    };

    onSmsChange(e) {
        let mpayment = this.state.payment;
        mpayment.sms = mpayment.sms === 1 ? 0 : 1;
        this.setState({ payment: mpayment });
    };

    onSubmit(e) {
        e.preventDefault();

        if (this.validate()) {
            return false;
        }

        // let mpayment = {
        //     organization_id: this.state.payment.organization_id,
        //     payment_code: this.state.payment.payment_code,
        //     phone_code: this.state.payment.phone_code,
        // }
        let mpayment = this.state.payment;
        mpayment.amount = parseInt(mpayment.amount);

        Axios.post(buildURL(`/payment`), mpayment)
            .then((response) => {
                successAlert("Amount received");
                //window.history.back();
                this.resetData();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    resetData() {
        let { payment, error } = this.state;
        Object.keys(payment).forEach((key) => {
            if(key!="member_id"){
                payment[key] = "";
            }
          
        });
        Object.keys(error).forEach((key) => {
            error[key] = [];
        });
        this.setState({ error: error });
    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mpayment = this.state.payment;
        Object.keys(mpayment).forEach((key) => {
            mpayment[key] = "";
        });
        this.setState({ payment: mpayment });
    }

    validate() {
        let mpayment = this.state.payment;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mError).forEach((key) => {
            if (key === "cheque_number") {
                if (mpayment.payment_type !== "cash" && mpayment.payment_type !== "NACH" && (mpayment[key] === null || mpayment[key] === "")) {
                    mError[key] = "Reference number Should not be empty";
                    isError = true;
                } else {
                    mError[key] = "";
                }
            } else if (key === "cheque_date") {
                if (mpayment.payment_type === "cheque" && (mpayment[key] === null || mpayment[key] === "")) {
                    mError[key] = "Date Should not be empty";
                    isError = true;
                } else {
                    mError[key] = "";
                }
            } else if (key !== "member_id" && mpayment[key] === "") {
                mError[key] = key + " Should not be empty";
                isError = true;
            } else {
                mError[key] = "";
            }
        });
        this.setState({ error: mError });
        return isError;

    }

    deleteItem() {
        Axios.delete(buildURL_v1(MASTER_BANK + "/" + this.props.id))
            .then((response) => {
                successAlert("Deleted successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    render() {

        const { payment, error, organizationList, bankList, memberList } = this.state;

        let organization = organizationList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });
        let member = memberList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });
        let bank = bankList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let scheme_type = donationType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let payment_type = paymentType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });

        const defaultProps = {
            options: organization,
            getOptionLabel: option => organization.name,
        };

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={8}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                       

                                <Grid container spacing={8}>

                                    <Grid item xs={6}>

                                        <FormControl fullWidth error={error.organization_id.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Organization</InputLabel>
                                            <Select
                                                name='organization_id'
                                                fullWidth
                                                value={payment.organization_id}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {organization}</Select>
                                            <FormHelperText>{error.organization_id}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6}>

                                        <FormControl fullWidth error={error.bank_name.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Bank</InputLabel>
                                            <Select
                                                name='bank_name'
                                                fullWidth
                                                value={payment.bank_name}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {bank}</Select>
                                            <FormHelperText>{error.bank_name}</FormHelperText>
                                        </FormControl>

                                    </Grid>

                                    <Grid item xs={6}>
                                        <FormControl fullWidth error={error.scheme_type.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Donation type</InputLabel>
                                            <Select
                                                name='scheme_type'
                                                fullWidth
                                                value={payment.scheme_type}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {scheme_type}</Select>
                                            <FormHelperText>{error.scheme_type}</FormHelperText>
                                        </FormControl>

                                    </Grid>
                        
                                    <Grid item xs={6}>
                                        <FormControl fullWidth error={error.payment_type.length > 0 ? true : false} >
                                            <InputLabel htmlFor="name-error">Payment type</InputLabel>
                                            <Select
                                                name='payment_type'
                                                fullWidth
                                                value={payment.payment_type}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                displayEmpty
                                            >
                                                {payment_type}</Select>
                                            <FormHelperText>{error.payment_type}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    {payment.payment_type === "cheque" || payment.payment_type === "online" ?
                                        <Grid item xs={6}>
                                            <TextField
                                                name='cheque_number'
                                                label='Reference No'
                                                fullWidth
                                                helperText={error.cheque_number}
                                                value={payment.cheque_number}
                                                error={error.cheque_number.length > 0 ? true : false}
                                                onChange={(e) => this.onHandleChange(e)}
                                            />
                                        </Grid>
                                        : ""
                                    }
                                    {payment.payment_type === "cheque" ?
                                        <Grid item xs={6}>
                                            <TextField
                                                name='cheque_date'
                                                label='Date on Cheque'
                                                fullWidth
                                                type="date"
                                                helperText={error.cheque_date}
                                                value={payment.cheque_date}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                error={error.cheque_date.length > 0 ? true : false}
                                                onChange={(e) => this.onHandleChange(e)}
                                            />
                                        </Grid>
                                        : ""
                                    }
                                    <Grid item xs={6}>
                                        <TextField
                                            name='amount'
                                            label='Amount'
                                            fullWidth
                                            type="number"
                                            helperText={error.amount}
                                            value={payment.amount}
                                            error={error.amount.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            name='payment_date'
                                            label='Date'
                                            fullWidth
                                            type="date"
                                            helperText={error.payment_date}
                                            value={payment.payment_date}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            error={error.payment_date.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                    </Grid>
                                </Grid>


                            <CardActions>
                                <Grid container justify="flex-end" >
                                    <Grid item xs={6}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={payment.sms === 1}
                                                    onChange={(e) => this.onSmsChange(e)}
                                                    value="checkedB"
                                                    color="primary"

                                                />
                                            }
                                            label="Send sms"
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Grid container justify="flex-end" >
                                            <Button
                                                variant='raised'
                                                style={{ margin: '8px' }}
                                                onClick={(e) => { window.history.back() }}
                                            >Cancel</Button>
                                            <Button
                                                variant='raised'
                                                color="primary"
                                                style={{ margin: '8px' }}
                                                onClick={(e) => this.onSubmit(e)}
                                            >Receive</Button>
                                        </Grid>
                                    </Grid>
                                </Grid>

                            </CardActions>

                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default MemberPaymentAdd;