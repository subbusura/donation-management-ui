import React from 'react';
import { List, Datagrid, Filter, Pagination, ChipField, ReferenceInput, downloadCSV, TextInput, TextField, SelectInput } from 'react-admin';
import { SUPER_USER, USER_ACCOUNT, getDistricts } from '../constants/Constants';
import DateFilter from '../payment/DateFilter';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';

const MemberFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <SelectInput lable="Status" source="status" choices={[
            { id: 10, name: 'Active' },
            { id: 6, name: 'Inactive' },
            { id: 5, name: 'Pending' },
        ]}
            allowEmpty={false}
        />
        <DateFilter
            source="date_range"
            fromLabel="From"
            toLabel="To"
            fromSource="date_range.from"
            toSource="date_range.to"
            label="Created"
        />

        <SelectInput lable="Type" source="profile_type" choices={[

            { id: 'individual', name: 'Individual' },
            { id: 'company', name: 'Company' },
        ]}
            allowEmpty={false}
        />
        {localStorage.getItem(SUPER_USER) === "1" ?
            <ReferenceInput label="District" source="city" reference="master/city" allowEmpty={false} perPage={500} >
                <SelectInput optionText="name" source="name" optionValue="name"
                    FormHelperTextProps={{ classes: { root: 'hide-helper' } }}
                />
            </ReferenceInput>
            :
            <SelectInput
                label="District"
                source="city"
                choices={getDistricts(localStorage.getItem(USER_ACCOUNT))}
                allowEmpty={false}
            />
        }
        <ReferenceInput label="Reference" source="referenced_by" reference="user" allowEmpty={false} perPage={500} >
            <SelectInput optionText="first_name" source="name"
                FormHelperTextProps={{ classes: { root: 'hide-helper' } }}
            />
        </ReferenceInput>
    </Filter>
);
const MemberPagination = props => <Pagination rowsPerPageOptions={[50, 100, 200]} {...props} />

const exporter = member => {
    const exportableMember = member.map((item)=>{
        item.registered = item.created_at;
        item.mobile_number = "+"+item.phone_code+" "+item.mobile_number;
        item.whatsapp_number = "+"+item.phone_code+" "+item.whatsapp_number;
        return item;
    })
    const csv = convertToCSV({
        data: exportableMember,
        fields: ['profile_type', 'name', 'email', 'mobile_number', 'whatsapp_number', 'city', 'state', 'country', 'registered']
    });
    downloadCSV(csv, 'Members');
}

const MemberList = props => (

    <List filters={<MemberFilter />} {...props} exporter={exporter} sort={{ field: 'name', order: 'DESC' }} pagination={<MemberPagination />}
        bulkActionButtons={false}
        perPage={100}
    >
        <Datagrid rowClick="edit" >
            {/* <TextField source="id" /> */}
            <ChipField source="profile_type" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="mobile_number" />
            <TextField source="whatsapp_number" />
            <TextField source="city" />
            <TextField source="state" />
            <TextField source="country" />
            <TextField source="status" />
            <TextField source="created_at" />
            {/* <EditButton /> */}
        </Datagrid>
    </List>
);

export default MemberList;
