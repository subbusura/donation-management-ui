import React from 'react';
import { FileInput, FileField } from 'ra-ui-materialui/lib/input';
import { SimpleForm } from 'ra-ui-materialui/lib/form';
import { Button, Toolbar, Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { SaveButton } from 'ra-ui-materialui/lib/button';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import { file } from '@babel/types';
import Axios from 'axios';
import { buildURL } from '../constants/api';
import DeleteIcon from '@material-ui/icons/Delete';
import DownloadIcon from '@material-ui/icons/Archive';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import ImageIcon from '@material-ui/icons/Image';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { validateResponse } from '../constants/ResponseValidation';
import { SUPER_USER, ADMIN } from '../constants/Constants';

const toolbarStyles = {
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between',
    },
};

const CustomToolbar = withStyles(toolbarStyles)(props => (
    <Toolbar {...props}>
        {/* <SaveButton disabled /> */}
    </Toolbar>
));

class MemberDocs extends React.Component {

    constructor(props) {
        super(props);

        this.fileInputRef = React.createRef();
        this.imageInputRef = React.createRef();

        this.state = {
            docList: []
        }
    }

    componentDidMount() {
        this.getDocuments();
    }

    getDocuments() {
        Axios.get(buildURL(`/member/${this.props.id}/documents`))
            .then((res) => {
                this.setState({ docList: res.data });
            })
            .catch((error) => {

            });

    }

    handleChange(files) {
        commonLog("Files", files.target.files[0]);
        this.uploadFile(files.target.files[0]);
    }

    uploadFile(file) {
        const formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("type", "form-62");
        commonLog(file, file.name);
        commonLog(formData);
        Axios.post(buildURL(`/member/1/documents`), formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((res) => {
            console.log("Res", res);
            this.getDocuments();
        }).catch((error) => {
            console.log("Error", error);
        })
    }

    getPdfFiles() {
        let list = [];
        list = this.state.docList.filter((item) => {
            if (item.location.endsWith(".pdf")) {
                return item;
            }
        });
        return list;
    }

    getImageFiles() {
        let list = [];
        list = this.state.docList.filter((item) => {
            if (!item.location.endsWith(".pdf")) {
                return item;
            }
        });
        return list;
    }

    onPdfDownload(e, item) {
        e.preventDefault();
    }

    onPdfDelete(e, item) {
        e.preventDefault();
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this file?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deletePdf(item) }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    onImageDownload(e, item) {
        e.preventDefault();
    }

    onImageDelete(e, item) {
        e.preventDefault();
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this image?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deletePdf(item) }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    deletePdf(item) {
        Axios.delete(buildURL(`/member/${this.props.id}/documents/${item.id}`))
            .then((res) => {
                successAlert("deleted successfully");
                this.deleteItem(item);
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    deleteItem(mItem) {
        let docList = [];
        docList = this.state.docList.filter((item) => {
            if (mItem.id !== item.id) {
                return item;
            }
        });
        this.setState({ docList: docList });
    }

    render() {

        let pdfFiles = this.getPdfFiles();
        let imageFiles = this.getImageFiles();
        console.log(pdfFiles, imageFiles);

        return (
            // <SimpleForm toolbar={<CustomToolbar />} >

            //     <FileInput source="files" label="Related files" accept=".pdf" onChange={(e)=>this.handleChange(e)} >
            //         <FileField source="src" title="title"  onChange={(e)=>this.handleChange(e)} />
            //     </FileInput>

            // </SimpleForm>
            <Grid>

                {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1" ?
                    <div>
                        <input
                            ref={this.fileInputRef}
                            style={{ display: "none" }}
                            type='file'
                            accept=".pdf"
                            onChange={this.handleChange.bind(this)} />
                        <Button
                            variant='raised'
                            color='primary'
                            onClick={(e) => { e.preventDefault(); this.fileInputRef.current.click() }}
                        >
                            <PdfIcon style={{ width: '16px', height: '16px', marginRight: '8px' }} />
                            PDF Upload</Button>

                        <input
                            ref={this.imageInputRef}
                            style={{ display: "none" }}
                            type='file'
                            accept="image/*"
                            onChange={this.handleChange.bind(this)} />
                        <Button
                            variant='raised'
                            color='secondary'
                            style={{ marginLeft: '16px' }}
                            onClick={(e) => { e.preventDefault(); this.imageInputRef.current.click() }}
                        >
                            <ImageIcon style={{ width: '16px', height: '16px', marginRight: '8px' }} />
                            Image Upload</Button>
                    </div>
                    : ""
                }


                <Grid item xs={12} style={{ marginTop: '32px' }} >
                    <Typography variant='subheading' style={{ marginBottom: '32px' }} >Files</Typography>
                    {pdfFiles.length > 0 ? this.renderPdfFiles(pdfFiles) : <Typography style={{ marginLeft: '32px' }} variant="caption">Empty</Typography>}
                </Grid>

                <Grid item xs={12} style={{ marginTop: '32px' }} >
                    <Typography variant='subheading' style={{ marginBottom: '32px' }} >Images</Typography>
                    {imageFiles.length > 0 ? this.renderImages(imageFiles) : <Typography style={{ marginLeft: '32px' }} variant="caption">Empty</Typography>}
                </Grid>

            </Grid>

        )
    }

    renderImages(Images) {
        return (
            <Grid container spacing={12}>
                {
                    Images.map((item) => {
                        console.log('http://dm.nekhop.com' + item.location)
                        return (
                            <Grid item xs={2}>
                                <img src={'http://dm.nekhop.com' + item.location} />
                                <Grid item xs={12}>
                                    {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1" ?
                                        <div>
                                            <a href="#" onClick={(e) => this.onImageDownload(e, item)} ><DownloadIcon color='secondary' style={{ width: '16px', height: '16px' }} /></a>
                                            <a href="#" onClick={(e) => this.onImageDelete(e, item)} ><DeleteIcon color='error' style={{ width: '16px', height: '16px' }} /></a>
                                        </div>
                                        : ""
                                    }
                                </Grid>
                            </Grid>
                        )
                    })
                }
            </Grid>
        );
    }
    renderPdfFiles(files) {
        return (
            <Grid container spacing={12}>
                {
                    files.map((item) => {
                        return (
                            <Grid item xs={2}>
                                <img src='/images/pdf.png' />

                                {localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1" ?
                                    <div>
                                        <a href={item.location} download={item.location} ><Typography noWrap variant="caption">{item.location}</Typography></a>
                                        <a href="#" onClick={(e) => this.onPdfDownload(e, item)} ><DownloadIcon color='secondary' style={{ width: '16px', height: '16px' }} /></a>
                                        <a href="#" onClick={(e) => this.onPdfDelete(e, item)} ><DeleteIcon color='error' style={{ width: '16px', height: '16px' }} /></a>
                                    </div> : ""
                                }
                            </Grid>
                        )
                    })
                }
            </Grid>
        );
    }
}

export default MemberDocs;