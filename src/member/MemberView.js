import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, InputLabel, Select, MenuItem, FormHelperText, FormControl, Typography, Paper, IconButton, Tabs, Tab, Icon, Avatar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import { buildURL } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import EmailIcon from '@material-ui/icons/Email';
import ContactIcon from '@material-ui/icons/ContactPhone';
import CakeIcon from '@material-ui/icons/Cake';
import HomeIcon from '@material-ui/icons/Home';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import MemberSubscription from './MemberSubscription';
import { memberTabs, memberTabPages, SUPER_USER } from '../constants/Constants';
import { Link } from 'ra-ui-materialui';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

const iconItem = {

}

class MemberView extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.match.params;
        this.state = {
            member: null,
            isLoading: false,
            selectedPath: this.params.path,
            subscription: ''

        }
    }

    componentDidMount() {
        this.getMember();
        this.getMemberSubscription();
    }

    getMember() {
        Axios.get(buildURL(`/member/${this.params.id}`))
            .then((res) => {
                this.setState({ member: res.data });

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getMemberSubscription() {
        Axios.get(buildURL(`/member/${this.params.id}/subscription`))
            .then((res) => {
                this.setState({ subscription: res.data });

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    deleteMember() {
        Axios.delete(buildURL(`/member/${this.params.id}`))
            .then((res) => {
                successAlert("Member deleted successfully");
                window.history.back();

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onDelete(e) {
        e.preventDefault();

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to delete this member?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { this.deleteMember() }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });

    }


    render() {

        const { member } = this.state;

        return (
            <Grid justify='flex-start' alignItems='flex-start' >
                <Typography variant='title'>Profile View</Typography>

                <Grid item xs={11}>
                    {
                        member !== null ? this.renderView(member) : ""
                    }
                </Grid>
            </Grid>
        )

    }


    handleTabChange(e, value) {
        let selectedPath = "";
        memberTabPages.map((item, index) => {
            if (item.value === value) {
                selectedPath = item.path;
                //return <item.component />;
            }
        });
        this.setState({ selectedPath: selectedPath });
    }

    onSubscriptionUpdate(item) {
        this.setState({ subscription: item });
    }

    getTab() {
        let value = 0;
        memberTabPages.map((item, index) => {
            if (this.state.selectedPath === item.path) {
                value = index;
            }
        });
        return value;
    }

    getSelectedPage() {
        //let value = this.renderGeneral();
        let value = <MemberSubscription id={this.params.id} subscription={this.state.subscription} onUpdateSuccess={(item) => this.onSubscriptionUpdate(item)} />
        memberTabPages.map((item, index) => {
            if (index !== 0)
                if (this.state.selectedPath === item.path) {
                    if (item.value === 0)
                        value = <item.component id={this.params.id} subscription={this.state.subscription} onUpdateSuccess={(item) => this.onSubscriptionUpdate(item)} />
                    else
                        value = <item.component id={this.params.id} />
                }
        });
        return value;
    }

    renderView(member) {
        let tabs = memberTabs.map((item, index) => {
            return <Tab
                key={index}
                component={Link}
                to={`/member/${this.params.id}/${item.label}`}
                label={item.label}
                style={{ color: '#000000' }}
            />
        })
        return (
            <Grid>

                <Paper square elevation={1} style={{ marginTop: '16px', padding: '8px' }} >

                    <Grid container spacing={8}>

                        <Grid item xs={2}>
                            <Grid container justify="center" alignItems="center">
                                <Avatar src='/images/profile1.png' alt="" style={{ width: 150, height: 150 }} />
                            </Grid>
                        </Grid>
                        <Grid item xs={9}>
                            {/* <Typography variant='title'>{member.name}</Typography>
                            <Typography variant='body2'>{member.profile_type}</Typography> */}
                            {this.renderGeneral()}
                        </Grid>
                        {localStorage.getItem(SUPER_USER) === "1" ?
                            <Grid item xs={1}>
                                <IconButton component={Link} to={`/member/${this.params.id}/update`} > <EditIcon /> </IconButton>
                                <IconButton> <DeleteIcon onClick={(e) => this.onDelete(e)} /> </IconButton>
                            </Grid>
                            : ""
                        }
                        {/* <Grid item xs={1}>
                            <IconButton> <DeleteIcon /> </IconButton>
                        </Grid> */}

                    </Grid>

                </Paper>

                <Paper square elevation={1} style={{ marginTop: '16px', padding: '16px' }}>
                    <Tabs
                        value={this.getTab()}
                        onChange={(e, value) => this.handleTabChange(e, value)}
                        aria-label="Vertical tabs example"
                    >
                        {tabs}
                    </Tabs>

                    {this.renderTabItem()}
                </Paper>

            </Grid>
        )
    }

    renderTabItem() {
        return (
            <Grid style={{ margin: '16px' }} >
                {this.getSelectedPage()}
            </Grid>
        )
    }

    renderGeneral() {
        let { member } = this.state;
        return (
            <Grid container style={{ margin: '8px' }} >
                <Grid container >
                    <Typography variant='title'>{member.name}</Typography>
                </Grid>
                <Grid container >
                    <Typography variant='body2'>{member.profile_type}</Typography>
                </Grid>
                <Grid container style={{ marginTop: '16px' }} >
                    <EmailIcon style={{ width: '16px', height: '16px', marginRight: '8px', color: '#9e9e9e' }} />
                    <Grid item xs={11}><Typography>{member.email}</Typography></Grid>
                </Grid>
                <Grid container style={{ marginTop: '8px' }} >
                    <ContactIcon style={{ width: '16px', height: '16px', marginRight: '8px', color: '#9e9e9e' }} />
                    <Grid item xs={11}><Typography>{member.mobile_number}</Typography></Grid>
                </Grid>
                <Grid container style={{ marginTop: '8px' }} >
                    <CakeIcon style={{ width: '16px', height: '16px', marginRight: '8px', color: '#9e9e9e' }} />
                    <Grid item xs={11}><Typography>{member.dob}</Typography></Grid>
                </Grid>
                <Grid container style={{ marginTop: '8px' }} >
                    <HomeIcon style={{ width: '16px', height: '16px', marginRight: '8px', color: '#9e9e9e' }} />
                    <Grid item xs={11}>
                        <Typography>
                            {
                                member.address_line_one + ", " + member.address_line_two + ", " +
                                member.area + ", " +
                                member.city + ", " +
                                member.state + ", " +
                                member.country + "."
                            }
                        </Typography>
                    </Grid>
                </Grid>

            </Grid>
        )
    }

    renderSubscription() {
        return <MemberSubscription />
    }

}

export default MemberView;