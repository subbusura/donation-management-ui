import React from 'react';
import { Grid, Typography, IconButton, TextField, Button, Table, TableBody, TableHead, TableRow, TableCell } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import { buildURL } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import EditIcon from '@material-ui/icons/Edit';
import moment from 'moment';
import { Link } from 'react-router-dom';


class MemberPayment extends React.Component {

    constructor(props) {
        super(props);
        this.id = this.props.id;
        this.state = {
            payment: [],
            isLoading: false,
            isUpdate: false,

        }
    }

    componentDidMount() {
        this.getPaymentList();
    }

    componentWillReceiveProps(nextProps) {
    }

    getPaymentList() {
        Axios.get(buildURL(`/member/${this.id}/payment`))
            .then((res) => {
                this.setState({ payment: res.data.items });
            })
            .catch((error) => {

            });
    }

    render() {

        const { subscription } = this.state;

        return (
            <Grid justify='flex-start' alignItems='flex-start' >

                <Grid item xs={11}>
                    {
                        subscription !== null ? this.renderView() : ""
                    }
                </Grid>
            </Grid>
        )

    }

    handleTabChange(e, index) {
        this.setState({ selectedTab: index });
       
    }

    renderView() {
        return (
            <Grid>

                {this.renderTable()}

            </Grid>
        )
    }

    onReceiptDelete(e,item){
        e.preventDefault();
        Axios.delete(buildURL(`/member/${this.id}/payment/receipt-delete/${item.id}`))
        .then((res) => {
            this.getPaymentList()
        })
        .catch((error) => {

        });
    }

    renderTable() {

        let mItems = this.state.payment.map((item, index) => {
            return (
                <TableRow>
                    <TableCell>{item.amount}</TableCell>
                    <TableCell>{item.bank_name}</TableCell>
                    <TableCell>{item.organization_id}</TableCell>
                    <TableCell>{item.scheme_type}</TableCell>
                    <TableCell>{item.payment_type}</TableCell>
                    <TableCell>{item.received_datetime}</TableCell>
                    <TableCell>
                        {
                           item.status==10 ? <a type="button" href={item.receipt_location} target="_blank"><Button color="secondary" size="small"> Receipt </Button></a>:""
                        }
                    </TableCell>
                    <TableCell>{item.status==10?"Received":"Pending"}</TableCell>
                    <TableCell>
                        {
                            <Button color="danger" size="small" onClick={(e)=>this.onReceiptDelete(e,item)} > Delete </Button>
                        }
                    </TableCell>
                </TableRow>
            )
        })

        return (
            <Grid container spacing={16} style={{ marginTop: '32px' }} >

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Amount</TableCell>
                            <TableCell align="right">Bank Name</TableCell>
                            <TableCell align="right">Organization</TableCell>
                            <TableCell align="right">Donation Type</TableCell>
                            <TableCell align="right">Paymant Type</TableCell>
                            <TableCell align="right">Received</TableCell>
                            <TableCell align="right">Receipt</TableCell>
                            <TableCell align="right">Status</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {mItems}
                    </TableBody>
                </Table>


            </Grid>
        )
    }

}

export default MemberPayment;