import React,{Fragment} from 'react';
import { change } from 'redux-form';
import { Create, FormDataConsumer,REDUX_FORM_NAME,SelectInput,ReferenceInput ,Edit, SimpleForm, Toolbar,DisabledInput, TextInput, DateInput, LongTextInput, ReferenceManyField, Datagrid, TextField, DateField, EditButton } from 'react-admin';
import {PostSaveButton} from './PostSaveButton';
import Axios from 'axios';
import { buildURL, buildURL_v1, COUNTRY, MEMBER } from './../constants/api';
const PostEditToolbar = props => (
    <Toolbar {...props} >
        <PostSaveButton />
    </Toolbar>
);

export default (props) => (
    <Create {...props} >
    <SimpleForm>
    <TextInput source="title" />
    <TextInput source="teaser" options={{ multiLine: true }} />
    <FormDataConsumer>
        {({ formData, dispatch,getSource, ...rest }) => (
            <Fragment>
                <ReferenceInput label="Country" source="country" reference="master/country" onChange={value =>{
                          dispatch(change(REDUX_FORM_NAME, 'state', ''))
                          dispatch(change(REDUX_FORM_NAME, 'city', ''))
                    }}  {...rest}>
                 <SelectInput
                    optionText="name"
                    optionValue="country_code"
                    />
                  </ReferenceInput>

                  <ReferenceInput label="State" source="state" filter={{country_code:formData.country,is_depend:1}} reference="master/state" onChange={value =>{       
                        dispatch(change(REDUX_FORM_NAME, 'city', ''))
                    }}  {...rest}>

                <SelectInput
                    optionText="name"
                    optionValue="id"
                    />
                  </ReferenceInput>

                   
                  <ReferenceInput label="City" source="city" filter={{state_id:formData.state,is_depend:1}} reference="master/city" {...rest}>

                <SelectInput
                    optionText="name"
                    optionValue="id"
                    />
                  </ReferenceInput>
            </Fragment>
        )}
    </FormDataConsumer>
    <DateInput label="Publication date" source="published_at" defaultValue={new Date()} />
    </SimpleForm>
    </Create>
);
