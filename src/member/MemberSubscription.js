import React from 'react';
import { Grid, Typography, IconButton, TextField, Button, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import { buildURL } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import EditIcon from '@material-ui/icons/Edit';
import { recurringType, SUPER_USER } from '../constants/Constants';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { confirmAlert } from 'react-confirm-alert';


class MemberSubscription extends React.Component {

    constructor(props) {
        super(props);
        this.id = this.props.id;
        this.state = {
            subscription: null,
            subscriptionItem: {
                start: '',
                interval: '',
                type: '',
                amount: '',
                status: 1
            },
            error: {
                start: [],
                interval: [],
                type: [],
                amount: []
            },
            isLoading: false,
            isUpdate: false,
            status: 0,

        }
    }

    componentDidMount() {
        //this.getMemberSubscription();
        this.setState({ subscription: this.props.subscription, subscriptionItem: this.props.subscription, status: this.props.subscription.status });
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ subscription: nextProps.subscription, subscriptionItem: nextProps.subscription, status: nextProps.subscription.status });
    }

    getMemberSubscription() {
        Axios.get(buildURL(`/member/${this.id}/subscription`))
            .then((res) => {
                this.setState({ subscription: res.data, subscriptionItem: res.data, status: res.data.status });

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onHandleChange(e) {
        let { subscriptionItem } = this.state;
        subscriptionItem[e.target.name] = e.target.value;
        this.setState({ subscriptionItem: subscriptionItem });
    }

    onEditClick(e) {
        e.preventDefault();
        this.setState({ isUpdate: true });
    }

    onUpdate(e) {
        e.preventDefault();
        if (this.validate()) {
            return false;
        }

        Axios.put(buildURL(`/member/${this.id}/subscription`), this.state.subscriptionItem)
            .then((response) => {
                successAlert("Subscription updated successfully");
                this.setState({ isUpdate: false });
                this.props.onUpdateSuccess(response.data);
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });
    }

    validate() {
        let subscriptionItem = this.state.subscriptionItem;
        let mError = this.state.error;
        let isError = false;

        Object.keys(subscriptionItem).forEach((key) => {
            if (subscriptionItem[key] === "") {
                mError[key].push(this.formatString(key) + " cannot not be empty");
                isError = true;
            } else {
                mError[key] = [];
            }
        });
        this.setState({ error: mError });
        return isError;
    }

    onUpdateCancel(e) {
        e.preventDefault();
        this.setState({ isUpdate: false });
    }

    render() {

        const { subscription } = this.state;

        return (
            <Grid justify='flex-start' alignItems='flex-start' >

                <Grid item xs={11}>
                    {
                        subscription !== null ? this.renderView() : ""
                    }
                </Grid>
            </Grid>
        )

    }

    handleTabChange(e, index) {
        this.setState({ selectedTab: index });
    }

    onStatusChange(e) {
        e.preventDefault();
        let { status } = this.state;
        let tStatus = status === 1 ? "Do you want to disble subscription for this user?" : "Do you want to enable subscription for this user?";
        confirmAlert({
            title: 'Confirm!',
            message: tStatus,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        this.changeStatus(status === 1 ? 0 : 1)
                    }
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    changeStatus(status) {
        Axios.post(buildURL(`/member/${this.id}/subscription/status`))
            .then((response) => {
                let { subscriptionItem } = this.state;
                subscriptionItem.status = status;
                this.setState({ status: status, subscriptionItem: subscriptionItem });
            })
            .catch((error) => {
                validateResponse(error.response);
            })
    }

    renderView() {
        return (
            <Grid>

                {this.state.isUpdate ? this.renderUpdateForm() : this.renderSubscriptionView()}

            </Grid>
        )
    }

    renderSubscriptionView() {
        let { subscription, status } = this.state;
        console.log("Sub", subscription);
        return (
            <Grid container style={{ marginTop: '16px' }} >

                <Grid container style={{ marginBottom: '16px' }} >
                    <Grid item xs={10}>
                        {localStorage.getItem(SUPER_USER) === "1" ?
                            <Grid container justify='space-between' alignItems='center' >

                                <FormControlLabel
                                    value="start"
                                    control={<Switch color="primary" />}
                                    label="Status"
                                    labelPlacement="start"
                                    checked={status === 1}
                                    onChange={(e) => this.onStatusChange(e)}
                                />
                                <IconButton onClick={(e) => this.onEditClick(e)} > <EditIcon /> </IconButton>
                            </Grid>
                            : ""
                        }
                    </Grid>
                </Grid>

                <Grid container >
                    <Grid item xs={2}><Typography variant='body2' >Subscribed At</Typography></Grid>
                    <Grid item xs={4}><Typography variant='subheading' >{": " + subscription.start}</Typography></Grid>
                    {/* </Grid>
                <Grid container style={{ marginTop: '16px' }} > */}
                    <Grid item xs={2}><Typography variant='body2' >Amount</Typography></Grid>
                    <Grid item xs={4}><Typography variant='subheading' >{": Rs." + subscription.amount}</Typography></Grid>
                </Grid>
                <Grid container style={{ marginTop: '16px' }} >
                    <Grid item xs={2}><Typography variant='body2' >Subscription Type</Typography></Grid>
                    <Grid item xs={4}><Typography variant='subheading' >{": " + subscription.interval + " " + subscription.type + " once"}</Typography></Grid>
                    {/* </Grid>
                <Grid container style={{ marginTop: '16px' }} > */}
                    <Grid item xs={2}><Typography variant='body2' >Next Due</Typography></Grid>
                    <Grid item xs={4}><Typography variant='subheading' >{": " + subscription.next_due}</Typography></Grid>
                </Grid>
            </Grid >
        )
    }

    renderUpdateForm() {
        let { subscriptionItem, error } = this.state;

        let donation = recurringType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        return (
            <Grid container justify='center' alignItems='center' spacing={16} style={{ marginTop: '32px' }} >
                <Grid item xs={5}>
                    <FormControl fullWidth error={error.type.length > 0 ? true : false} >
                        <InputLabel htmlFor="age-simple">Recurring Type</InputLabel>
                        <Select
                            name='type'
                            label='Recurring type'
                            fullWidth
                            helperText={error.type}
                            value={subscriptionItem.type}
                            error={error.type.length > 0 ? true : false}
                            onChange={(e) => this.onHandleChange(e)}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            displayEmpty>
                            {donation}
                        </Select>
                    </FormControl>

                    <TextField
                        name='interval'
                        label='Scheme interval'
                        fullWidth
                        type="number"
                        helperText={error.interval}
                        value={subscriptionItem.interval}
                        error={error.interval.length > 0 ? true : false}
                        //required
                        onChange={(e) => this.onHandleChange(e)}
                        style={{ marginTop: '16px' }}
                    />

                    <TextField
                        name='amount'
                        label='Scheme amount'
                        fullWidth
                        type="number"
                        helperText={error.amount}
                        value={subscriptionItem.amount}
                        error={error.amount.length > 0 ? true : false}
                        onChange={(e) => this.onHandleChange(e)}

                    />

                    <TextField
                        name='start'
                        label='Scheme Start time'
                        fullWidth
                        type='date'
                        helperText={error.start}
                        value={subscriptionItem.start}
                        error={error.start.length > 0 ? true : false}
                        onChange={(e) => this.onHandleChange(e)}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />

                    {/* <TextField
                        name='type'
                        label='Scheme type'
                        fullWidth
                        helperText={error.type}
                        value={subscriptionItem.type}
                        error={error.type.length > 0 ? true : false}
                        //required
                        onChange={(e) => this.onHandleChange(e)}
                    /> */}
                    {/* <TextField
                        name='interval'
                        label='Scheme interval'
                        fullWidth
                        type="number"
                        helperText={error.interval}
                        value={subscriptionItem.interval}
                        error={error.interval.length > 0 ? true : false}
                        //required
                        onChange={(e) => this.onHandleChange(e)}
                    /> */}
                </Grid>
                {/* <Grid item xs={3}>
                    <TextField
                        name='amount'
                        label='Scheme amount'
                        fullWidth
                        type="number"
                        helperText={error.amount}
                        value={subscriptionItem.amount}
                        error={error.amount.length > 0 ? true : false}
                        onChange={(e) => this.onHandleChange(e)}
                    />

                    <TextField
                        name='start'
                        label='Scheme Start time'
                        fullWidth
                        type='date'
                        helperText={error.start}
                        value={subscriptionItem.start}
                        error={error.start.length > 0 ? true : false}
                        onChange={(e) => this.onHandleChange(e)}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid> */}
                <Grid container justify='flex-end' >
                    <Grid item xs={6}>
                        <Button onClick={(e) => this.onUpdateCancel(e)} variant="contained" style={{ marginRight: '8px' }} >Cancel</Button>
                        <Button onClick={(e) => this.onUpdate(e)} variant="contained" color="primary" className='pull-right'>Update</Button>
                    </Grid>
                </Grid>
            </Grid>
        )
    }

    renderSubscription() {
        return "Subscription";
    }

}

export default MemberSubscription;