import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, InputLabel, Select, MenuItem, FormHelperText, FormControl, Checkbox, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, buildURL_v1, COUNTRY, MEMBER } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import { Z_STREAM_ERROR } from 'zlib';
import { profileType, gender } from '../constants/Constants';


class MemberUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.match.params;
        this.state = {
            member: {
                name: '',
                profile_type: "",
                email: "",
                mobile_number: "",
                address_line_one: "",
                address_line_two: "",
                country: "",
                state: "",
                city: "",
                area: "",
                zip_code: "",
                dob: '',
                gender: '',
                country_code: '',
                state_id: '',
                referenced_by: '',
                whatsapp_number: "",
                is_whatsapp: 0
            },
            error: {
                name: [],
                profile_type: [],
                mobile_number: [],
                email: [],
                address_line_one: [],
                address_line_two: [],
                country: [],
                country_code: [],
                state: [],
                city: [],
                area: [],
                zip_code: [],
                dob: [],
                gender: [],
                referenced_by: [],
                whatsapp_number: [],
            },
            countryList: [],
            stateList: [],
            cityList: [],
            areaList: [],
            userList: [],
            isLoading: false,
            isStateReload: true,
            isCityReload: true,
            isAreaReload: true,
        }
    }

    componentDidMount() {
        this.getCountryList();
        this.getMember();
    }

    getMember() {
        Axios.get(buildURL(`/member/${this.params.id}`))
            .then((res) => {

                let state = { name: res.data.state };
                let city = { name: res.data.city };
                let area = { name: res.data.area };
                let { member } = this.state;
                Object.keys(member).forEach((key) => {
                    member[key] = res.data[key];
                })

                this.setState({ member: member, stateList: [state], cityList: [city], areaList: [area] });

            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getCountryList() {
        Axios.all([
            Axios.get(buildURL(COUNTRY)),
            Axios.get(buildURL("/user", { "per-page": 0 }))
        ])
            .then(Axios.spread((countryList, userList) => {
                this.setState({ countryList: countryList.data });
                this.setState({ userList: userList.data.items });
            }))
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onWhatsappChange(e) {
        let member = this.state.member;
        member.is_whatsapp = member.is_whatsapp === 1 ? 0 : 1;
        member.whatsapp_number = member.mobile_number;
        this.setState({ member: member });
    }

    onHandleChange(e) {

        // console.log("Res", res.data)
        //         let countryCode = this.getCountry(res.data.country);
        //         console.log("State ID", countryCode)
        //         if (countryCode !== null) {
        //             this.getStateList(countryCode);
        //         }
        //         let state_id = this.getState(res.data.state);
        //         console.log("State ID", state_id)
        //         if (state_id !== null) {
        //             this.getCityList(state_id);
        //         }

        let member = this.state.member;

        if (member.is_whatsapp === 1 && e.target.name === "mobile_number") {
            member[e.target.name] = e.target.value;
            member.whatsapp_number = e.target.value;
        } else {
            member[e.target.name] = e.target.value;
        }

        if (e.target.name === 'country') {
            console.log("Handle change ", "Country")
            let countryCode = this.getCountry(e.target.value);
            if (countryCode !== null) {
                member.country_code = countryCode;
                member.state = "";
                member.city = "";
                member.area = "";
                this.getStateList(countryCode, true);
            }
        }

        if (e.target.name === 'state') {
            let state_id = this.getState(e.target.value);
            if (state_id !== null) {
                member.city = "";
                member.state_id = state_id.state_id;
                this.getCityList(state_id);
            }
        }

        if (e.target.name === 'city') {
            let city_id = this.getCity(e.target.value);
            if (city_id !== null) {
                member.area = "";
                member.city_id = city_id.city_id;
                this.getAreaList(city_id);
            }
        }

        this.setState({ member: member });
    };

    getCountry(country) {
        for (let i = 0; i < this.state.countryList.length; i++) {
            if (country === this.state.countryList[i].name) {
                return this.state.countryList[i].country_code
            }
        }
        return null;
    }

    getState(state) {
        for (let i = 0; i < this.state.stateList.length; i++) {
            if (state === this.state.stateList[i].name) {
                return { country_code: this.state.stateList[i].country_code, state_id: this.state.stateList[i].id }
            }
        }
        return null;
    }

    getCity(state) {
        for (let i = 0; i < this.state.cityList.length; i++) {
            if (state === this.state.cityList[i].name) {
                return { country_code: this.state.cityList[i].country_code, state_id: this.state.cityList[i].state_id, city_id: this.state.cityList[i].id }
            }
        }
        return null;
    }

    fetchState() {
        if (this.state.isStateReload)
            this.getStateList(this.state.member.country_code)
    }

    fetchCity() {
        let { member } = this.state;
        if (this.state.isCityReload)
            this.getCityList({ country_code: member.country_code, state_id: member.state_id });
    }

    fetchArea() {
        let { member } = this.state;
        if (this.state.isAreaReload)
            this.getAreaList({ country_code: member.country_code, state_id: member.state_id, city_id: member.city_id });
    }

    getStateList(countryCode) {

        Axios.get(buildURL(`${COUNTRY}/${countryCode}/state`))
            .then((res) => {
                this.setState({ stateList: res.data, isStateReload: false });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getCityList(state) {
        Axios.get(buildURL(`${COUNTRY}/${state.country_code}/state/${state.state_id}/city`))
            .then((res) => {
                this.setState({ cityList: res.data, isCityReload: false });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getAreaList(state) {
        Axios.get(buildURL(`${COUNTRY}/${state.country_code}/state/${state.state_id}/city/${state.city_id}/area`))
            .then((res) => {
                this.setState({ areaList: res.data, isAreaReload: false });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(this.state.member)

        if (this.validate()) {
            return false;
        }

        Axios.put(buildURL(`${MEMBER}/${this.params.id}`), this.state.member)
            .then((response) => {
                successAlert("Updated successfully");
                window.history.back();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mmember = this.state.member;
        Object.keys(mmember).forEach((key) => {
            mmember[key] = "";
        });
        this.setState({ member: mmember });
    }

    validate() {
        let mmember = this.state.member;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mmember).forEach((key) => {
            if (key !== "dob" && key !== "address_line_two" && key !== "gender") {
                if (mmember[key] === "") {
                    mError[key].push(this.formatString(key) + " cannot not be empty");
                    isError = true;
                } else {
                    mError[key] = [];
                }
            }
        });
        this.setState({ error: mError });
        return isError;
    }

    formatString(key) {
        let mKey = key.slice(0, 1).toUpperCase() + key.slice(1, key.length);
        return mKey.replace("_", " ").replace("_", " ");
    }

    render() {

        const { member, error } = this.state;
        console.log("MEMEBER", member)
        let profile_type = profileType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let mGender = gender.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let country = this.state.countryList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let state = this.state.stateList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let city = this.state.cityList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let area = this.state.areaList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let users = this.state.userList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.first_name + " " + item.last_name}</MenuItem>
            )
        });
        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={10}>
                    <form onSubmit={(e) => this.onSubmit(e)}>

                        <Grid container spacing={8}>
                            <Grid item xs={6}>
                                <Card>

                                    <CardHeader title='Update Member'></CardHeader>

                                    <CardContent>

                                        <TextField
                                            name='name'
                                            label='Name'
                                            fullWidth
                                            helperText={error.name}
                                            value={member.name}
                                            error={error.name.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <TextField
                                            name='email'
                                            label='Email'
                                            fullWidth
                                            helperText={error.email}
                                            value={member.email}
                                            error={error.email.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <Grid container spacing={8}>

                                            <Grid item xs={6}>

                                                <TextField
                                                    name='mobile_number'
                                                    label='Mobile number'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.mobile_number}
                                                    value={member.mobile_number}
                                                    error={error.mobile_number.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                />

                                            </Grid>
                                            <Grid item xs={6}>
                                                <TextField
                                                    name='whatsapp_number'
                                                    label='WhatsApp number'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.whatsapp_number}
                                                    value={member.whatsapp_number}
                                                    error={error.whatsapp_number.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                />
                                                <FormControlLabel
                                                    style={{ marginTop: '-40px' }}
                                                    control={
                                                        <Checkbox
                                                            checked={member.is_whatsapp === 1}
                                                            onChange={(e) => this.onWhatsappChange(e)}
                                                            value="checkedB"
                                                            color="primary"
                                                        // icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                                                        // checkedIcon={<CheckBoxIcon fontSize="small" />}

                                                        />
                                                    }
                                                    label="Same as mobile number"
                                                />

                                            </Grid>
                                        </Grid>

                                        <Grid container spacing={8}>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.profile_type.length > 0 ? true : false} >
                                                    <InputLabel htmlFor="name-error">Profile Type</InputLabel>
                                                    <Select
                                                        name='profile_type'
                                                        label='Profile Type'
                                                        fullWidth
                                                        value={member.profile_type}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {profile_type}</Select>
                                                    <FormHelperText>{error.profile_type}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth >
                                                    <InputLabel htmlFor="age-simple">Gender</InputLabel>
                                                    <Select
                                                        name='gender'
                                                        label='Gender'
                                                        fullWidth
                                                        helperText={error.gender}
                                                        value={member.gender}
                                                        error={error.gender.length > 0 ? true : false}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        displayEmpty
                                                    >
                                                        {mGender}</Select>
                                                </FormControl>
                                            </Grid>

                                        </Grid>

                                        <TextField
                                            name='dob'
                                            label='Date of birth'
                                            fullWidth
                                            type='date'
                                            helperText={error.dob}
                                            value={member.dob}
                                            error={error.dob.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            style={{ marginTop: '16px' }}
                                        />

                                        <FormControl fullWidth >
                                            <InputLabel htmlFor="age-simple">Reference</InputLabel>
                                            <Select
                                                name='referenced_by'
                                                fullWidth
                                                helperText={error.referenced_by}
                                                value={member.referenced_by}
                                                error={error.referenced_by.length > 0 ? true : false}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                displayEmpty
                                            >
                                                {users}</Select>
                                        </FormControl>

                                    </CardContent>

                                </Card>
                            </Grid>
                            <Grid item xs={6}>
                                <Card style={{ marginTop: '0px' }} >

                                    <CardHeader title='Address'></CardHeader>

                                    <CardContent>

                                        <TextField
                                            name='address_line_one'
                                            label='Address line one'
                                            fullWidth
                                            helperText={error.address_line_one}
                                            value={member.address_line_one}
                                            error={error.address_line_one.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <TextField
                                            name='address_line_two'
                                            label='Address line two'
                                            fullWidth
                                            helperText={error.address_line_two}
                                            value={member.address_line_two}
                                            error={error.address_line_two.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                        />

                                        <Grid container spacing={8}>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.country.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">Country</InputLabel>
                                                    <Select
                                                        name='country'
                                                        label='Country'
                                                        fullWidth
                                                        value={member.country}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {country}</Select>
                                                    <FormHelperText>{error.country}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.state.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">State</InputLabel>
                                                    <Select
                                                        name='state'
                                                        label='State'
                                                        fullWidth
                                                        value={member.state}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        displayEmpty
                                                        onClick={(e) => this.fetchState()}
                                                    >
                                                        {state}</Select>
                                                    <FormHelperText>{error.state}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                        </Grid>

                                        <Grid container spacing={8} style={{ marginTop: '16px' }} >

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.city.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">District</InputLabel>
                                                    <Select
                                                        name='city'
                                                        label='City'
                                                        fullWidth
                                                        value={member.city}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                        onClick={(e) => this.fetchCity()}
                                                    >
                                                        {city}</Select>
                                                    <FormHelperText>{error.city}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.area.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">City / Area</InputLabel>
                                                    <Select
                                                        name='area'
                                                        label='Area'
                                                        fullWidth
                                                        value={member.area}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                        onClick={(e) => this.fetchArea()}
                                                    >
                                                        {area}</Select>
                                                    <FormHelperText>{error.area}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                        </Grid>
                                        <TextField
                                            name='zip_code'
                                            label='ZIP Code'
                                            fullWidth
                                            helperText={error.zip_code}
                                            value={member.zip_code}
                                            error={error.zip_code.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />

                                    </CardContent>

                                    <CardActions>
                                        <Grid container justify="flex-end" alignItems="right" >
                                            <Button
                                                variant='raised'
                                                style={{ margin: '8px' }}
                                                onClick={(e) => { window.history.back() }}
                                            >Cancel</Button>
                                            <Button
                                                variant='raised'
                                                color="primary"
                                                style={{ margin: '8px' }}
                                                onClick={(e) => this.onSubmit(e)}
                                            >Update</Button>
                                        </Grid>

                                    </CardActions>

                                </Card>
                            </Grid>
                        </Grid>

                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default MemberUpdate;