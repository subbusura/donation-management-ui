import React ,{useCallback,useState} from 'react';
import { Button, Toolbar, Grid, Typography } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Axios from 'axios';
import { buildURL } from '../constants/api';

const MemberImport = (props) => {
    
     const fileInputRef =React.useRef();

     const [result , setResult] = useState({error:0,success:0,total:0})
     const [status,setStatus] = useState("Waiting For Upload")
     const [merrors,setMerrors] = useState([]);
     const HandleUploadClick = useCallback((e)=>{
            e.preventDefault();
            fileInputRef.current.click()
            setStatus([])
            setMerrors([])
     })

     const HandleFileChange = useCallback((e)=>{

        setStatus("Upload Begin")
         const file = e.target.files[0];
         const formData = new FormData();
         formData.append("file", file, file.name);
         

         Axios.post(buildURL(`/member/bulk/import`), formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((res) => {

            setStatus("Upload Success")

            setResult({
                 total:res.data.total_record,
                 error:res.data.error.length,
                 success:res.data.success.length
            })

            setMerrors(res.data.error)
       
        }).catch((error) => {

            setStatus("Upload Error")
            console.log("Error", error);
        })

     })


    return (
    <div>
            <div>
                        
                        <input
                                    ref={fileInputRef}
                                    style={{ display: "none" }}
                                    type='file'
                                    accept=".csv"
                                    onChange={HandleFileChange}
                                    />
                                <Button
                                    variant='raised'
                                    color='primary'
                                    onClick={HandleUploadClick}
                                >
                                    <CloudUploadIcon style={{ width: '16px', height: '16px', marginRight: '8px' }} />
                                    Member Bulk Upload</Button>
                </div>
        
            <div>
                <h2>Upload Status :  {status}</h2>

                     <ul>
                             <li>
                                Total Record : {result.total}
                            </li>
                            <li>
                                Success : {result.success}
                            </li>
                            <li>
                                Error : {result.error}
                            </li>
                        </ul>

               
            <h2>Error Messages</h2>
                <table>
                     <tr>
                         <th>Row Id</th>
                         <th>message</th>
                     </tr>
                     {
                         merrors.map((value,index)=>{

                            return <tr>
                                    <td>{index+1}</td>
                                     <td>{JSON.stringify(value)}</td>
                            </tr>

                         })
                     }
                     {
                         merrors.length==0?"No Errors":""
                     }
                     
                </table>

                
            </div>
    </div>
    )
};
export default MemberImport;