import React from 'react';
import { Card, CardContent, Grid, CardHeader, TextField, CardActions, Button, InputLabel, Select, MenuItem, FormHelperText, FormControl, Checkbox, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextInput, SimpleForm, LongTextInput, Create } from 'react-admin';
import Axios from 'axios';
import { buildURL, buildURL_v1, COUNTRY, MEMBER } from './../constants/api';
import { validateResponse } from './../constants/ResponseValidation';
import { validate } from '@babel/types';
import { commonLog, successAlert, errorAlert } from '../common/Common';
import { Z_STREAM_ERROR } from 'zlib';
import { profileType, gender, donationType, recurringType } from '../constants/Constants';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';


class MemberCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            member: {
                name: '',
                profile_type: "",
                email: "",
                mobile_number: "",
                whatsapp_number: "",
                address_line_one: "",
                address_line_two: "",
                country: "",
                state: "",
                city: "",
                area: '',
                zip_code: "",
                dob: '',
                gender: '',
                country_code: '',
                recurring_start: '',
                recurring_interval: '',
                recurring_type: '',
                recurring_amount: '',
                referenced_by: '',
                is_whatsapp: 0
            },
            error: {
                name: [],
                profile_type: [],
                mobile_number: [],
                whatsapp_number: [],
                email: [],
                address_line_one: [],
                address_line_two: [],
                country: [],
                country_code: [],
                state: [],
                city: [],
                area: [],
                zip_code: [],
                dob: [],
                gender: [], recurring_start: [],
                recurring_interval: [],
                recurring_type: [],
                recurring_amount: [],
                referenced_by: []
            },
            countryList: [],
            stateList: [],
            cityList: [],
            areaList: [],
            userList: [],
            isLoading: false
        }
    }

    componentDidMount() {
        this.getCountryList();
    }

    getCountryList() {
        Axios.all([
            Axios.get(buildURL(COUNTRY, { "per-page": 0 })),
            Axios.get(buildURL("/user", { "per-page": 0 }))
        ])
            .then(Axios.spread((countryList, userList) => {
                this.setState({ countryList: countryList.data });
                this.setState({ userList: userList.data.items });
            }))
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onWhatsappChange(e) {
        //e.preventDefault();
        let member = this.state.member;
        member.is_whatsapp = member.is_whatsapp === 1 ? 0 : 1;
        member.whatsapp_number = member.mobile_number;
        this.setState({ member: member });
    }

    onHandleChange(e) {
        let member = this.state.member;

        if (member.is_whatsapp === 1 && e.target.name === "mobile_number") {
            member[e.target.name] = e.target.value;
            member.whatsapp_number = e.target.value;
        } else {
            member[e.target.name] = e.target.value;
        }

        if (e.target.name === 'country') {
            let countryCode = this.getCountry(e.target.value);
            if (countryCode !== null) {
                member.country_code = countryCode;
                this.getStateList(countryCode);
            }
        }

        if (e.target.name === 'state') {
            let state_id = this.getState(e.target.value);
            if (state_id !== null) {
                this.getCityList(state_id);
            }
        }

        if (e.target.name === 'city') {
            let city_id = this.getCity(e.target.value);
            if (city_id !== null) {
                this.getAreaList(city_id);
            }
        }

        this.setState({ member: member });
    };

    getCountry(country) {
        for (let i = 0; i < this.state.countryList.length; i++) {
            if (country === this.state.countryList[i].name) {
                return this.state.countryList[i].country_code
            }
        }
        return null;
    }

    getState(state) {
        for (let i = 0; i < this.state.stateList.length; i++) {
            if (state === this.state.stateList[i].name) {
                return { country_code: this.state.stateList[i].country_code, state_id: this.state.stateList[i].id }
            }
        }
        return null;
    }

    getCity(state) {
        for (let i = 0; i < this.state.cityList.length; i++) {
            if (state === this.state.cityList[i].name) {
                return { country_code: this.state.cityList[i].country_code, state_id: this.state.cityList[i].state_id, city_id: this.state.cityList[i].id }
            }
        }
        return null;
    }

    getStateList(countryCode) {
        Axios.get(buildURL(`${COUNTRY}/${countryCode}/state`, { "per-page": 1000 }))
            .then((res) => {
                this.setState({ stateList: res.data });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getCityList(state) {
        Axios.get(buildURL(`${COUNTRY}/${state.country_code}/state/${state.state_id}/city`, { "per-page": 0 }))
            .then((res) => {
                this.setState({ cityList: res.data });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    getAreaList(state) {
        Axios.get(buildURL(`${COUNTRY}/${state.country_code}/state/${state.state_id}/city/${state.city_id}/area`, { "per-page": 0 }))
            .then((res) => {
                this.setState({ areaList: res.data });
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                errorAlert(response.message);
            });
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(this.state.member)

        if (this.validate()) {
            return false;
        }

        Axios.post(buildURL(MEMBER), this.state.member)
            .then((response) => {
                successAlert("Member created successfully");
                this.onSuccess();
            })
            .catch((error) => {
                let response = validateResponse(error.response);
                if (response.isValidation) {
                    //errorAlert(error.response.data.errors[0].message);
                    this.setError(error.response.data);
                } else {
                    errorAlert(response.message);
                }
            });

    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    onSuccess() {
        let mmember = this.state.member;
        Object.keys(mmember).forEach((key) => {
            mmember[key] = "";
        });
        this.setState({ member: mmember });
    }

    validate() {
        let mmember = this.state.member;
        let mError = this.state.error;
        let isError = false;

        Object.keys(mmember).forEach((key) => {
            if (key !== "dob" && key !== "address_line_two" && key !== "gender") {
                if (mmember[key] === "") {
                    mError[key].push(this.formatString(key) + " cannot not be empty");
                    isError = true;
                } else {
                    mError[key] = [];
                }
            }
        });
        this.setState({ error: mError });
        return isError;
    }

    formatString(key) {
        let mKey = key.slice(0, 1).toUpperCase() + key.slice(1, key.length);
        return mKey.replace("_", " ").replace("_", " ");
    }

    render() {

        const { member, error } = this.state;

        let profile_type = profileType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let mGender = gender.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let country = this.state.countryList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let state = this.state.stateList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let city = this.state.cityList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let area = this.state.areaList.map((item, index) => {
            return (
                <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
            )
        });
        let donation = recurringType.map((item, index) => {
            return (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
            )
        });
        let users = this.state.userList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.first_name + " " + item.last_name}</MenuItem>
            )
        });

        return (
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={10}>
                    <form onSubmit={(e) => this.onSubmit(e)}>

                        <Grid container spacing={8}>
                            <Grid item xs={6}>
                                <Card>

                                    <CardHeader title='Create Member'></CardHeader>

                                    <CardContent>

                                        <TextField
                                            name='name'
                                            label='Name'
                                            fullWidth
                                            helperText={error.name}
                                            value={member.name}
                                            error={error.name.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <TextField
                                            name='email'
                                            label='Email'
                                            fullWidth
                                            helperText={error.email}
                                            value={member.email}
                                            error={error.email.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <Grid container spacing={8}>

                                            <Grid item xs={12} md={6} lg={6} xl={6} >
                                                <TextField
                                                    name='mobile_number'
                                                    label='Mobile number'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.mobile_number}
                                                    value={member.mobile_number}
                                                    error={error.mobile_number.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                />

                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6} xl={6} >
                                                <TextField
                                                    name='whatsapp_number'
                                                    label='WhatsApp number'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.whatsapp_number}
                                                    value={member.whatsapp_number}
                                                    error={error.whatsapp_number.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                    style={{ marginBottom: '-35px' }}
                                                />

                                                {/* <span style={{ fontSize: '12px' }} >
                                                    <input
                                                        type='checkbox'
                                                        checked={member.is_whatsapp === 0}
                                                        onChange={(e) => this.onWhatsappChange(e)}
                                                        value="checkedB"
                                                        color="primary"
                                                    ></input>
                                                    Same as mobile number
                                                </span> */}


                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={member.is_whatsapp === 1}
                                                            onChange={(e) => this.onWhatsappChange(e)}
                                                            value="checkedB"
                                                            color="primary"
                                                            inputProps={{
                                                                width: 100,
                                                                height: 100
                                                            }}
                                                            icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                                                            checkedIcon={<CheckBoxIcon fontSize="small" />}

                                                        />
                                                    }
                                                    label="Same as mobile number"
                                                />
                                            </Grid>
                                        </Grid>

                                        <Grid container spacing={8} style={{ marginTop: '-10px' }} >

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.profile_type.length > 0 ? true : false} >
                                                    <InputLabel htmlFor="name-error">Profile Type</InputLabel>
                                                    <Select
                                                        name='profile_type'
                                                        label='Profile Type'
                                                        fullWidth
                                                        value={member.profile_type}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {profile_type}</Select>
                                                    <FormHelperText>{error.profile_type}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth >
                                                    <InputLabel htmlFor="age-simple">Gender</InputLabel>
                                                    <Select
                                                        name='gender'
                                                        label='Gender'
                                                        fullWidth
                                                        helperText={error.gender}
                                                        value={member.gender}
                                                        error={error.gender.length > 0 ? true : false}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        displayEmpty
                                                    >
                                                        {mGender}</Select>
                                                </FormControl>
                                            </Grid>

                                        </Grid>

                                        <TextField
                                            name='dob'
                                            label='Date of birth'
                                            fullWidth
                                            type='date'
                                            helperText={error.dob}
                                            value={member.dob}
                                            error={error.dob.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            style={{ marginTop: '16px' }}
                                        />

                                        <FormControl fullWidth >
                                            <InputLabel htmlFor="age-simple">Reference</InputLabel>
                                            <Select
                                                name='referenced_by'
                                                fullWidth
                                                helperText={error.referenced_by}
                                                value={member.referenced_by}
                                                error={error.referenced_by.length > 0 ? true : false}
                                                required
                                                onChange={(e) => this.onHandleChange(e)}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                displayEmpty
                                            >
                                                {users}</Select>
                                        </FormControl>

                                    </CardContent>

                                </Card>

                                <Card style={{ marginTop: '16px' }} >

                                    <CardHeader title='Subscription'></CardHeader>

                                    <CardContent>

                                        <FormControl fullWidth error={error.recurring_type.length > 0 ? true : false} >
                                            <InputLabel htmlFor="age-simple">Recurring Type</InputLabel>
                                            <Select
                                                name='recurring_type'
                                                label='Recurring type'
                                                fullWidth
                                                helperText={error.recurring_type}
                                                value={member.recurring_type}
                                                error={error.recurring_type.length > 0 ? true : false}
                                                onChange={(e) => this.onHandleChange(e)}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                displayEmpty
                                            >
                                                {donation}
                                            </Select>
                                        </FormControl>
                                        <Grid container spacing={8}>

                                            <Grid item xs={6}>
                                                <TextField
                                                    name='recurring_interval'
                                                    label='interval'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.recurring_interval}
                                                    value={member.recurring_interval}
                                                    error={error.recurring_interval.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                    style={{ marginTop: '16px' }}
                                                />
                                            </Grid>
                                            <Grid item xs={6}>
                                                <TextField
                                                    name='recurring_amount'
                                                    label='Amount'
                                                    fullWidth
                                                    type="number"
                                                    helperText={error.recurring_amount}
                                                    value={member.recurring_amount}
                                                    error={error.recurring_amount.length > 0 ? true : false}
                                                    onChange={(e) => this.onHandleChange(e)}
                                                    style={{ marginTop: '16px' }}
                                                />
                                            </Grid>
                                        </Grid>
                                        <TextField
                                            name='recurring_start'
                                            label='Start time'
                                            fullWidth
                                            type='date'
                                            helperText={error.recurring_start}
                                            value={member.recurring_start}
                                            error={error.recurring_start.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            style={{ marginTop: '8px' }}
                                        />

                                    </CardContent>

                                </Card>

                            </Grid>
                            <Grid item xs={6}>
                                <Card style={{ marginTop: '0px' }} >

                                    <CardHeader title='Address'></CardHeader>

                                    <CardContent>

                                        <TextField
                                            name='address_line_one'
                                            label='Address line one'
                                            fullWidth
                                            helperText={error.address_line_one}
                                            value={member.address_line_one}
                                            error={error.address_line_one.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />
                                        <TextField
                                            name='address_line_two'
                                            label='Address line two'
                                            fullWidth
                                            helperText={error.address_line_two}
                                            value={member.address_line_two}
                                            error={error.address_line_two.length > 0 ? true : false}
                                            onChange={(e) => this.onHandleChange(e)}
                                        />

                                        <Grid container spacing={8}>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.country.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">Country</InputLabel>
                                                    <Select
                                                        name='country'
                                                        label='Country'
                                                        fullWidth
                                                        value={member.country}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {country}</Select>
                                                    <FormHelperText>{error.country}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.state.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">State</InputLabel>
                                                    <Select
                                                        name='state'
                                                        label='State'
                                                        fullWidth
                                                        value={member.state}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        displayEmpty
                                                    >
                                                        {state}</Select>
                                                    <FormHelperText>{error.state}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                        </Grid>

                                        <Grid container spacing={8} style={{ marginTop: '16px' }} >

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.city.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">District</InputLabel>
                                                    <Select
                                                        name='city'
                                                        label='City'
                                                        fullWidth
                                                        value={member.city}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {city}</Select>
                                                    <FormHelperText>{error.city}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <FormControl fullWidth error={error.city.length > 0 ? true : false}>
                                                    <InputLabel htmlFor="age-simple">City / Area</InputLabel>
                                                    <Select
                                                        name='area'
                                                        label='Area'
                                                        fullWidth
                                                        value={member.area}
                                                        required
                                                        onChange={(e) => this.onHandleChange(e)}
                                                        displayEmpty
                                                    >
                                                        {area}</Select>
                                                    <FormHelperText>{error.city}</FormHelperText>
                                                </FormControl>
                                            </Grid>

                                        </Grid>

                                        <TextField
                                            name='zip_code'
                                            label='ZIP Code'
                                            fullWidth
                                            helperText={error.zip_code}
                                            value={member.zip_code}
                                            error={error.zip_code.length > 0 ? true : false}
                                            required
                                            onChange={(e) => this.onHandleChange(e)}
                                        />

                                    </CardContent>

                                    <CardActions>
                                        <Grid container justify="flex-end" alignItems="right" >
                                            <Button
                                                variant='raised'
                                                style={{ margin: '8px' }}
                                                onClick={(e) => { window.history.back() }}
                                            >Cancel</Button>
                                            <Button
                                                variant='raised'
                                                color="primary"
                                                style={{ margin: '8px' }}
                                                onClick={(e) => this.onSubmit(e)}
                                            >Create</Button>
                                        </Grid>

                                    </CardActions>

                                </Card>
                            </Grid>
                        </Grid>

                    </form>
                </Grid>
            </Grid>
        )

    }

}

export default MemberCreate;