import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import DollarIcon from '@material-ui/icons/AttachMoney';
import { Typography } from '@material-ui/core';
import CardIcon from './CardIcon';

class DashboardItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            item: null,
        }
    }

    componentDidMount() {
        this.setState({ item: this.props.item })
    }

    render() {

        let { item } = this.state;

        return (

            <div>
                {item !== null ?
                    <div>
                        <CardIcon Icon={item.icon} bgColor={item.bg_color} />
                        <Card style={{ overflow: 'inherit', textAlign: 'right', padding: '16px', minHeight: '80' }} >
                            <Typography color="textSecondary">
                                {item.title}
                            </Typography>
                            <Typography variant="headline" component="h2">
                                {item.value}
                            </Typography>
                        </Card>
                    </div> : ""
                }
            </div>

        )

    }

}

export default DashboardItem;