import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import DollarIcon from '@material-ui/icons/AttachMoney';
import { Typography, Grid, TextField, FormControl, InputLabel, Select, MenuItem, Input } from '@material-ui/core';
import CardIcon from './CardIcon';
import DashboardItem from './DashboardItem';
import PersonIcon from '@material-ui/icons/Person';
import PendingIcon from '@material-ui/icons/HourglassFull';
import ReceiveIcon from '@material-ui/icons/SystemUpdateAlt';
import Axios from 'axios';
import { buildURL, buildURL_v1 } from './../constants/api';
import PieChart from '../charts/PieChart';
import { validateResponse } from '../constants/ResponseValidation';
import { errorAlert } from '../common/Common';
import BarChart from '../charts/BarChart';
import { SUPER_USER, ADMIN, getDistricts, USER_ACCOUNT, timeInterval } from '../constants/Constants';
// var CanvasJSReact = require('./../canvasjs.react');
// var CanvasJS = CanvasJSReact.CanvasJS;
// var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.date = new Date();
        this.state = {
            item: [
                {
                    id: 1,
                    title: "Total donors",
                    value: 0,
                    bg_color: "#31708f",
                    icon: PersonIcon
                },
                {
                    id: 2,
                    title: "Received payment",
                    value: 0,
                    bg_color: "#e91e63",
                    icon: ReceiveIcon
                },
                {
                    id: 3,
                    title: "Pending payment",
                    value: 0,
                    bg_color: "#0288d1",
                    icon: PendingIcon
                },
            ],
            cityList: [],
            receivedAmount: [],
            organizationList: [],
            isAdmin: localStorage.getItem(SUPER_USER) === "1" || localStorage.getItem(ADMIN) === "1",
            filter: {
                type: "pending",
                timeInterval: "0",
                organization_id: "",
                date_range: {
                    from: this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).toString().slice(-2) + "-01",
                    to: this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).toString().slice(-2) + "-" + ("0" + this.date.getDate()).toString().slice(-2),
                }
            },
            isCustomDate: false,
            customDate: {
                from: '',
                to: ''
            }
        }
    }

    componentDidMount() {

        if (localStorage.getItem('user') === null || typeof localStorage.getItem('user') === 'undefined') {
            // localStorage.clear();
            // window.location.reload();
            return;
        }

        if (this.state.isAdmin) {

            Axios.get(buildURL_v1(`/master/city`, { "per-page": 0 }))
                .then((response) => {
                    let mCityList = response.data.items;
                    let tCity = [];
                    mCityList.forEach((item) => {
                        let cityItem = {
                            y: 0,
                            label: item.name,
                            name: item.name
                        }
                        tCity.push(cityItem);
                    })
                    this.setBarChartData(tCity, this.state.receivedAmount);
                    this.setState({ cityList: tCity });
                })
                .catch((error) => {

                });

        } else {
            let mCity = getDistricts(localStorage.getItem(USER_ACCOUNT));
            mCity.forEach((item) => {
                item["y"] = 0;
                item["label"] = item.name;
            })
            this.setState({ cityList: mCity });
        }

        this.getInitialData();

    }

    getInitialData() {
        let mFilter = { entry_date_range: this.state.filter.date_range };
        Axios.all([
            Axios.get(buildURL(`/dashboard/total/member`)),
            Axios.get(buildURL(`/dashboard/total/received`)),
            Axios.get(buildURL(`/dashboard/total/pending`)),
            Axios.get(buildURL(`/payment/pending`, { filter: JSON.stringify(mFilter) })),
            Axios.get(buildURL_v1(`/master/organization`, { filter: JSON.stringify({ "per-page": 100 }) })),
        ]).then(Axios.spread((member, received, pending, receivedList, organizationList) => {

            let mItem = this.state.item;
            mItem[0].value = member.data;
            mItem[1].value = received.data;
            mItem[2].value = pending.data;

            this.setBarChartData(this.state.cityList, receivedList.data.items);

            this.setState({ item: mItem, receivedAmount: receivedList.data.items, organizationList: organizationList.data.items });

        })).catch((error) => {
            let validation = validateResponse(error.response);
            errorAlert(validation.message);
        })
    }

    setBarChartData(cityList, receivedAmount) {

        let netAmount = 0;

        for (let i = 0; i < cityList.length; i++) {
            receivedAmount.forEach((item) => {
                if (cityList[i].name === item.member.city) {
                    netAmount = netAmount + parseFloat(item.amount);
                }
            });

            cityList[i]["y"] = netAmount;
            netAmount = 0;

        }

    }

    getPayment(type, dateRange, organization_id = this.state.filter.organization_id) {
        let url = "";
        let mFilter = {};
        if (type === "received") {
            url = "/payment";
            mFilter = { received_date_range: dateRange, organization_id: organization_id };
        } else {
            url = "/payment/pending";
            mFilter = { entry_date_range: dateRange };
        }
        Axios.get(buildURL(url, { filter: JSON.stringify(mFilter) }))
            .then((response) => {
                this.setBarChartData(this.state.cityList, response.data.items);
                this.setState({ receivedAmount: response.data.items })
            })
            .catch((error) => {
                let validation = validateResponse(error.response);
                errorAlert(validation.message);
            });
    }

    getPaymentList(organization) {
        let type = this.state.filter.type;
        let dateRange = this.state.filter.date_range;

        let url = "";
        let mFilter = {};
        if (type === "received") {
            url = "/payment";
            mFilter = { received_date_range: dateRange, organization_id: organization };
        } else {
            url = "/payment/pending";
            mFilter = { entry_date_range: dateRange };
        }
        Axios.get(buildURL(url, { filter: JSON.stringify(mFilter) }))
            .then((response) => {
                this.setBarChartData(this.state.cityList, response.data.items);
                this.setState({ receivedAmount: response.data.items })
            })
            .catch((error) => {
                let validation = validateResponse(error.response);
                errorAlert(validation.message);
            });
    }

    onFilterChange(e) {
        let { filter, isCustomDate, customDate } = this.state;
        filter.timeInterval = e.target.value;
        if (e.target.value === "custom") {
            isCustomDate = true;
        } else {
            customDate.from = "";
            customDate.to = "";
            isCustomDate = false;
            let interval = parseInt(e.target.value);

            let date1 = new Date().setMonth(this.date.getMonth() - interval);
            let startDate = new Date(date1);
            let endDate = new Date().setDate(0);
            let dateRange = { from: '', to: '' };

            if (interval === 0) {
                dateRange.from = this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).toString().slice(-2) + "-01";
                dateRange.to = this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).toString().slice(-2) + "-" + ("0" + this.date.getDate()).toString().slice(-2);
            } else if (interval === 1) {
                dateRange.from = startDate.getFullYear() + "-" + ("0" + (startDate.getMonth() + 1)).toString().slice(-2) + "-01";
                dateRange.to = new Date().getFullYear() + "-" + ("0" + (new Date().getMonth())).toString().slice(-2) + "-" + (new Date(endDate).getDate());
            } else {
                dateRange.from = startDate.getFullYear() + "-" + ("0" + (startDate.getMonth() + 1)).toString().slice(-2) + "-" + ("0" + this.date.getDate()).toString().slice(-2)
                dateRange.to = new Date().getFullYear() + "-" + ("0" + (new Date().getMonth() + 1)).toString().slice(-2) + "-" + ("0" + this.date.getDate()).toString().slice(-2);
            }
            this.getPayment(this.state.filter.type, dateRange);
            filter.date_range = dateRange;
        }

        this.setState({ filter: filter, isCustomDate: isCustomDate, customDate: customDate });
    }

    onTypeChange(e) {
        let { filter } = this.state;
        filter.type = e.target.value;
        filter.organization_id = "";
        this.getPayment(e.target.value, this.state.filter.date_range);
        this.setState({ filter: filter });
    }

    onCustomDateChange(e) {
        let { customDate, filter } = this.state;
        customDate[e.target.name] = e.target.value;

        if (customDate.from !== "" && customDate.to !== "") {
            let dateRange = { from: customDate.from, to: customDate.to };
            filter.date_range = dateRange;
            this.getPayment(this.state.filter.type, dateRange);
        }

        this.setState({ customDate: customDate, filter: filter });
    }

    onOrganizationChange(e) {
        let { filter } = this.state;
        filter.organization_id = e.target.value;
        this.getPaymentList(e.target.value);
        this.setState({ filter: filter });
    }

    render() {

        let { isCustomDate, customDate, organizationList } = this.state;

        let interval = timeInterval.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });

        let organization = organizationList.map((item, index) => {
            return (
                <MenuItem value={item.id} key={index}>{item.name}</MenuItem>
            )
        });

        return (

            <div>
                <Grid container spacing={0} >

                    <Grid item xs={12}>

                        <Grid container spacing={16}>

                            {
                                this.state.item.map((item, index) => {
                                    return (
                                        <Grid item xs={12} md={12} lg={4} xl={4} style={{ marginTop: '24px' }} >
                                            <DashboardItem item={item} />
                                        </Grid>
                                    )
                                })
                            }

                        </Grid>
                    </Grid>

                    <Grid item xs={12} style={{ marginTop: '32px' }} >
                        <Grid container spacing={16}>

                            <Grid item xs={2}>
                                <FormControl fullWidth >
                                    <InputLabel htmlFor="name-error">Duration</InputLabel>
                                    <Select
                                        name='timeInterval'
                                        fullWidth
                                        required
                                        onChange={(e) => this.onFilterChange(e)}
                                        displayEmpty
                                        value={this.state.filter.timeInterval}
                                    >
                                        {interval}
                                    </Select>
                                </FormControl>
                            </Grid>

                            {isCustomDate ?

                                <Grid item xs={4}>
                                    <Grid container spacing={16}>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                name="from"
                                                type='date'
                                                label="From"
                                                value={customDate.from}
                                                onChange={(e) => this.onCustomDateChange(e)}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                fullWidth
                                                name="to"
                                                type='date'
                                                label='To'
                                                value={customDate.to}
                                                onChange={(e) => this.onCustomDateChange(e)}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                                : ""
                            }

                            <Grid item xs={2}>
                                <FormControl fullWidth >
                                    <InputLabel htmlFor="name-error">Type</InputLabel>
                                    <Select
                                        name='type'
                                        fullWidth
                                        required
                                        onChange={(e) => this.onTypeChange(e)}
                                        displayEmpty
                                        value={this.state.filter.type}
                                    >
                                        <MenuItem value={"received"}>{"Received"}</MenuItem>
                                        <MenuItem value={"pending"}>{"Pending"}</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            {this.state.filter.type === "received" ?
                                <Grid item xs={2}>
                                    <FormControl fullWidth >
                                        <InputLabel htmlFor="name-error">Organization</InputLabel>
                                        <Select
                                            name='organization_id'
                                            fullWidth
                                            required
                                            onChange={(e) => this.onOrganizationChange(e)}
                                            displayEmpty
                                            value={this.state.filter.organization_id}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        >
                                            {/* <MenuItem value={""}>{"All"}</MenuItem> */}
                                            {organization}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                : ""
                            }

                            <Grid item xs={12}>
                                {this.state.cityList.length > 0 ?
                                    <BarChart data={this.state.cityList} title={this.state.filter} />
                                    : ""
                                }
                            </Grid>
                            {/* <Grid item xs={4}>
                                <PieChart />
                            </Grid> */}
                        </Grid>
                    </Grid>

                </Grid>
            </div >

        )

    }

}

export default Dashboard;